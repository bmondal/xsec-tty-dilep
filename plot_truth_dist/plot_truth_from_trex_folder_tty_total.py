#!/usr/bin/env python

"""
plot truth distribution from the trex-fitter Folded.py
"""
import ROOT
import os

var_dict= { 
    "tty2l_dPhill_all_syst":"dPhill",
    "tty2l_dEtall_all_syst":"dEtall",
    "tty2l_dr1_all_syst":"dr1",
    "tty2l_dr2_all_syst":"dr2",
    "tty2l_dr_all_syst":"dr",
    "tty2l_drlj_all_syst":"drlj",
    "tty2l_drphb_all_syst":"drphb",
    "tty2l_eta_all_syst":"eta",
    "tty2l_pt_all_syst":"pt",
    "tty2l_ptj1_all_syst":"ptj1",
    "tty2l_ptll_all_syst":"ptll"
    }


def get_last_folder_name(path):
  path = path.split("/")
  probably_last_element = path[len(path)-1]
  if "tty" in probably_last_element:
    return path[len(path)]
  else:
    return path[len(path) - 2] 


def plot(file_path, histname,save_name):
  ROOT.gROOT.SetStyle("ATLAS")
  file = ROOT.TFile(file_path)
  histogram = file.Get(histname)
  # add overflow in the last bin
  last_bin = histogram.GetNbinsX()
  histogram.SetBinContent(last_bin, histogram.GetBinContent(last_bin) + histogram.GetBinContent(last_bin+1))
  # divide by bin width
  for i in range(1, histogram.GetNbinsX() + 1):
      bin_content = histogram.GetBinContent(i)
      bin_error = histogram.GetBinError(i)
      bin_width = histogram.GetBinWidth(i)
      
      histogram.SetBinContent(i, bin_content / bin_width)
      histogram.SetBinError(i, bin_error / bin_width)
  canvas = ROOT.TCanvas()
  #canvas.SetRightMargin(0.09);
  #canvas.SetLeftMargin(0.15);
  #canvas.SetBottomMargin(0.15);
  canvas.cd()
  histogram.GetYaxis().SetTitleOffset(2.5)
  histogram.GetYaxis().SetTitle("Events / bin width")
  histogram.Draw("hist e")
  canvas.SaveAs(save_name)

def plot_for_this_trex_folder(path,var):
  file_path_prefix = path
  save_name_prefix = get_last_folder_name(file_path_prefix)
  file_path = "{}/UnfoldingHistograms/FoldedHistograms.root".format(file_path_prefix)
  histname = "{}_truth_distribution".format(var)
  save_name = "{}_Unfolding_truth_distribution.pdf".format(save_name_prefix)
  plot(file_path, histname, save_name)




if __name__ == "__main__":
  # path to the TRexFitter folder
  file_path_prefix = "../abs-xsec-with-trexfiles-local/v12/v35/syst-all-fit-data-mu-blinded/"
  # list of trex-fit folders 
  var_list = [ 
    "tty2l_dPhill_all_syst",
    "tty2l_dEtall_all_syst",
    "tty2l_dr1_all_syst",
    "tty2l_dr2_all_syst",
    "tty2l_dr_all_syst",
    "tty2l_drlj_all_syst",
    "tty2l_drphb_all_syst",
    "tty2l_eta_all_syst",
    "tty2l_pt_all_syst",
    "tty2l_ptj1_all_syst",
    "tty2l_ptll_all_syst"]
  #print(var_list)
  for var in var_list:
    path = "{}/{}/".format(os.path.abspath(file_path_prefix), var)
    print(">>>>>>>>>>>>>>>>>>>>> processing for folder {} <<<<<<<<<<<<<<<".format(path))
    plot_for_this_trex_folder(path,var)


