file_path = './syst-all/tty2l_pt_all_syst/Fits/tty2l_pt_all_syst.txt'
result_list = []

with open(file_path, 'r') as file:
    lines = file.readlines()
    for line in lines[7:]:
        if line.strip() == '':
            break
        else:
            first_word = line.split()[0]
            if "tty_dec-SigXsecOverSM" in first_word:  continue
            result_list.append(first_word)

print(result_list)


# find empty ranking files
import os
import random

folder_path = './syst-all/tty2l_pt_all_syst/Fits/'
files = [f for f in os.listdir(folder_path)]
empty_files = []

# Loop through all files and check if they're empty
for file in files:
    file_path = os.path.join(folder_path, file)
    if os.path.getsize(file_path) == 0:
        #print(f"{file} is empty")
        empty_files.append(file)

print(empty_files)

# loop over the np list and check in the empty list
empty_files_from_np = []
for np in result_list:
  tmp = "{}_{}_Unfolding_Bin_001_mu.txt".format("NPRanking",np)
  empty_files_from_np.append(tmp)
  #if any(tmp in file for file in empty_files):
  #  print("np: {} \t file: {} \n".format(np,file))

common_elements  = set(empty_files_from_np).intersection(set(empty_files))
print(common_elements)
print(len(common_elements))

import re
from subprocess import call
# Extract the np value from common_elements using regular expressions
empty_nps = []
for element in common_elements:
  match = re.match(r'^NPRanking_(\w+)_Unfolding_Bin_001_mu\.txt$', element)
  if match:
    np = match.group(1)
    empty_nps.append(np)

print(len(empty_nps))
for np in empty_nps:
  print(np)

submit = False
# submit jobs
if submit:
  for np in empty_nps:
    condor_sub_string = "pushd ./syst-all; condor_submit condor_tty2l_pt_all_syst.config."+np+".sub; popd"
    call(condor_sub_string, shell=True)
