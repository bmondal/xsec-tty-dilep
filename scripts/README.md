binning_variation -> this is for rebinning all the samples (putting manual binning or also there is automatic optimization of binning). This not only rebin the 1D histograms but also rebins 2D histograms as well as at the end calculates response matrices for the newly binned.

check_templates_by_checking_norm -> The script can calculate the normalization of every template for every systematic variation. If we have new version of histogram template, this can be compared with the previous version to check corrupted templates

copy_to_new_folder_and_rename_files -> The nominal folder should be untouched. We can copy 
