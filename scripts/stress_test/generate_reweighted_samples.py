#!/usr/bin/env python

"""
This scripts generate reweighted samples (histograms) from the nominal one
"""

import argparse
import ROOT
import  math

debug = False 

class ReweightSample:
  """
  Get data and MC root files
  for a particular histogram, calculate weight per bin 
        weight_i = 1 + Y*[(data_i - MC_i)/data_i ]; where i = bin index, Y = 1, -1
  """
  def __init__(self, rootfile_all_mc, rootfile_signal_mc, rootfile_data):
    self.rootfilename_all_data = rootfile_data
    self.rootfilename_all_mc = rootfile_all_mc
    self.rootfilename_signal_mc = rootfile_signal_mc
    self.outputfilename = ""
    self.file_all_data = ROOT.TFile(self.rootfilename_all_data)
    self.file_all_mc= ROOT.TFile(self.rootfilename_all_mc)
    self.file_signal_mc= ROOT.TFile(self.rootfilename_signal_mc)
    # store the hist name and the weights per bin so that it can be applied later for particle level histos
    self.weight_dict = {} # weight_dict = {"hist_name": list_containing_weights,... }

    if (debug): # if debug print status of the constructor
      print(" in __init__ \n")
      print(" rootfilename_mc: {} \n".format(self.rootfilename_signal_mc))
      print(" rootfilename_signal_mc: {} \n".format(self.rootfilename_signal_mc))
      print(" rootfilename_data: {} \n".format(self.rootfilename_all_data))


  def set_param_Y(self, param_Y):
    self.param_Y = param_Y

  def set_output_file_prefix(self, name):
    self.outputfilename = name

  def get_reweighted_hist_particle(self, hist_mc, hist_name):
    hist_mc_clone = hist_mc.Clone()
    if(hist_mc.Integral() == 0): return hist_mc_clone # if it is empty hist, just return the empty hist
    # in particle level "_reco_" is not "_part_"
    if "reco" in hist_name: return hist_mc_clone
    if "part" in hist_name:
      hist_name_modified = hist_name.replace("part", "reco") # why we doing this, is this for the easy to make plots later with same name?
    print("hist name before: {} ; after: {} \n".format(hist_name, hist_name_modified))
    # find the correct hist and index from the self.weight_dict
    weight_list = self.weight_dict[hist_name_modified]
    # loop over the bins and get get weighted hist
    for i in range(1, hist_mc.GetNbinsX() + 2):
      bin_content_mc = hist_mc.GetBinContent(i)
      
      reweighted_bin_content_mc = bin_content_mc * weight_list[i]
      hist_mc_clone.SetBinContent(i ,reweighted_bin_content_mc)
      if (debug): print("Bin: {} \t particle weight: {} ".format(i, weight_list[i]))

    return hist_mc_clone

  def get_reweighted_hist_reco(self, hist_mc, hist_name):
    hist_mc_clone = hist_mc.Clone()
    if(hist_mc.Integral() == 0): return hist_mc_clone # if it is empty hist, just return the empty hist
    # find the correct hist and index from the self.weight_dict
    weight_list = self.weight_dict[hist_name]
    # loop over the bins and get get weighted hist
    for i in range(1, hist_mc.GetNbinsX() + 2):
      bin_content_mc = hist_mc.GetBinContent(i)
      
      reweighted_bin_content_mc = bin_content_mc * weight_list[i]
      hist_mc_clone.SetBinContent(i ,reweighted_bin_content_mc)
      if (debug): print("Bin: {} \t particle weight: {} ".format(i, weight_list[i]))

    return hist_mc_clone


  """
  Calculate weight based on data and MC mismatch for different histograms
  at the end the weights are being saved in the self.weight_dict dictionary
  """
  def calculate_weight(self):
    directory = self.file_signal_mc.Get("Reco") # calculate the weight only for Reco directory histograms
    for key in directory.GetListOfKeys(): # loop over the histograms in the directory
      key_name = key.GetName()
      hist_mc = self.file_all_mc.Get("{}/{}".format("Reco", key_name))
      hist_data = self.file_all_data.Get("{}/{}".format("Reco", key_name))
      # don't do naything fro th2d
      if "h2" in key_name: continue
      # only do for reweighted /unfolding histos 
      if not "weighted" in key_name: continue
      # weighting Reco and particle is different. 
      if(hist_mc.Integral() == 0): continue
      # loop over the bins and get get weighted hist
      weight_list = [[1] * i for i in range(0,hist_mc.GetNbinsX()+2)]
      for i in range(1, hist_mc.GetNbinsX() + 2): # bin should start from 1 to 
        bin_content_mc = hist_mc.GetBinContent(i)
        bin_content_data = hist_data.GetBinContent(i)
        weight = 1
        if (bin_content_data != 0): weight = 1 + self.param_Y * math.fabs(bin_content_data - bin_content_mc)/bin_content_data # calculate weight here
        # update the weight in the weight list
        weight_list[i] = weight
      self.weight_dict["{}".format(key_name)] = weight_list


  """
  def get_reweighted_hist(self, hist_all_mc,hist_signal_mc, Tdir_name, hist_name):
    hist_data = self.file_all_data.Get("{}/{}".format(Tdir_name, hist_name))
    hist_mc_clone = hist_signal_mc.Clone()
    if(hist_signal_mc.Integral() == 0): return hist_mc_clone # if it is empty hist, just return the empty hist
    # loop over the bins and get get weighted hist
    weight_list = [[1] * i for i in range(0,hist_all_mc.GetNbinsX()+2)]
    for i in range(1, hist_all_mc.GetNbinsX() + 2): # bin should start from 1 to 
      bin_content_mc = hist_all_mc.GetBinContent(i)
      bin_content_data = hist_data.GetBinContent(i)
      weight = 1
      if (bin_content_data != 0): weight = 1 + self.param_Y * (bin_content_data - bin_content_mc)/bin_content_data # calculate weight here
      bin_content_signal_mc = hist_signal_mc.GetBinContent(i) # we want to reweight the signal MC only
      reweighted_bin_content_mc = bin_content_signal_mc * weight # set bin content with ttgamma_NLO here not the all_MCs
      hist_mc_clone.SetBinContent(i ,reweighted_bin_content_mc)
      if (debug): print("Bin: {} \t reco weight: {} ".format(i, weight))
      # update the weight in the weight list
      weight_list[i] = weight
    self.weight_dict["{}".format(hist_name)] = weight_list

    return hist_mc_clone
  """

  # create new rootfiles and update
  def save_in_rootfile(self):
    name_reweighted = "{}_reweighted_y{}.root".format(self.outputfilename, self.param_Y)
    file_reweighted = ROOT.TFile(name_reweighted, "RECREATE")
    """
    loop over Tdirectories->find histogram -> reweight
    """
    directory_list = ["Reco", "particle"]
    for dir in directory_list: 
      directory = self.file_signal_mc.Get(dir) # get Reco or particle dir
      file_reweighted.mkdir(dir) # create dir in outfile
      dir_obj_from_file_reweighted = file_reweighted.Get(dir) # this is the directory obj from the outfile. In this object we will write our new histos. this will be saved
      for key in directory.GetListOfKeys():
        key_name = key.GetName()
        histo_obj_signal_mc = self.file_signal_mc.Get("{}/{}".format(dir, key_name))
        # don't do naything fro th2d
        if "h2" in key_name: continue
        # only do for reweighted /unfolding histos 
        if not "weighted" in key_name: continue
        # weighting Reco and particle is different. 
        if(dir == "Reco"): 
          # don't do anything with the h2 hist
          hist_reweighted = self.get_reweighted_hist_reco(histo_obj_signal_mc, key_name)
          dir_obj_from_file_reweighted.WriteObject(hist_reweighted, "{}".format(key_name), "overwrite")
        if (dir == "particle"):
          hist_reweighted = self.get_reweighted_hist_particle(histo_obj_signal_mc,  key_name)
          dir_obj_from_file_reweighted.WriteObject(hist_reweighted, "{}".format(key_name), "overwrite")


if __name__=='__main__':
  # add normalization to a test.root file and in test_hist histogram and save as test_normalized.root
  parser = argparse.ArgumentParser()
  parser.add_argument("--rootfile_all_mc", type = str, help="name of the MC rootfile in which histogram contains")
  parser.add_argument("--rootfile_signal_mc", type = str, help="name of the MC rootfile in which histogram contains")
  parser.add_argument("--rootfile_all_data", type = str, help="name of the data rootfile in which histogram contains")
  parser.add_argument("--outfile", type = str, help="name of the outputfile, don't use .root at the end; because we will create up/down.root")
  parser.add_argument("--param_Y", type = float, help="value of param Y")
  args = parser.parse_args()

  obj = ReweightSample(args.rootfile_all_mc, args.rootfile_signal_mc, args.rootfile_all_data)
  obj.set_param_Y(args.param_Y)
  obj.set_output_file_prefix(args.outfile)
  obj.calculate_weight()
  obj.save_in_rootfile()
