DISPLAY=0
function run_stress_test_all_vars {
  #path_to_trex_fitted_folders="/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-dilep/abs-xsec-with-trexfiles-local/v12/v14/syst-all-xsec-20perct-offset/"
  path_to_trex_fitted_folders="/home/bm863639/xsec-tty-dilep/abs-xsec-with-trexfiles-local/v12/v13/stat-all/"
  path_to_nominal_unfolding_input="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/"
  var_name=$1
  path_linear_y1="$path_to_trex_fitted_folders/tty2l_${var_name}_all_stat_linear_reweighted_y1/"
  path_linear_y_1="$path_to_trex_fitted_folders/tty2l_${var_name}_all_stat_linear_reweighted_y-1/"
  path_nonlinear_y1="$path_to_trex_fitted_folders/tty2l_${var_name}_all_stat_nonlinear_reweighted_y1/"
  path_nonlinear_y_1="$path_to_trex_fitted_folders/tty2l_${var_name}_all_stat_nonlinear_reweighted_y-1/"
  path_truth="$path_to_nominal_unfolding_input/CR_sig/nominal_9999/"
  
  python plot_unfolded_truth.py  --path-trex-fit-linear-y1 $path_linear_y1  --path-trex-fit-linear-y-1 $path_linear_y_1 --path-trex-fit-nonlinear-y1 $path_nonlinear_y1 --path-trex-fit-nonlinear-y-1 $path_nonlinear_y_1 --path-truth  $path_truth
  }
run_stress_test_all_vars "pt"
run_stress_test_all_vars "eta"
run_stress_test_all_vars "dr"
run_stress_test_all_vars "dr1"
run_stress_test_all_vars "dr2"
run_stress_test_all_vars "dEtall"
run_stress_test_all_vars "dPhill"
run_stress_test_all_vars "ptll"
run_stress_test_all_vars "ptj1"
run_stress_test_all_vars "drphb"
run_stress_test_all_vars "drlj"
