path_unfolding_inputs="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/"
# Generate non linear reweighted smaples
function non_linear_reweight {
  CR="CR_sig"
  rootfile_all_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.all_MCs.root"
  rootfile_signal_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_prod.root" # if you want to reweight signal
  #rootfile_signal_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_dec.root" # wanted to reweight dec
  rootfile_data="$path_unfolding_inputs/$CR/nominal_9999/histograms.data.root"
  outfile="$path_unfolding_inputs/$CR/nominal_9999/ttgamma_prod" # if you want to weight tty_prod
  #outfile="$path_unfolding_inputs/$CR/nominal_9999/ttgamma_dec" # wanted to rewieght tty_dec

  # -----------
  # delete all_MCs(all the simulation added) and create again; (remember we need this for non-linear reweighting)
  pushd "$path_unfolding_inputs/CR_sig/nominal_9999/" || exit
  rm  histograms.all_MCs.root
  hadd histograms.all_MCs.root histograms.diboson.root histograms.efake.root histograms.hfake.root histograms.singletop.root histograms.ttbar.root histograms.ttgamma_dec.root histograms.ttgamma_prod.root histograms.ttv.root histograms.wgamma.root histograms.wjets.root histograms.Wt_inclusive.root histograms.Wty.root histograms.zgamma.root histograms.zjets.root
  popd || exit
  pushd "$path_unfolding_inputs/CR_bkg/nominal_9999/" || exit
  rm  histograms.all_MCs.root
  hadd histograms.all_MCs.root histograms.diboson.root histograms.efake.root histograms.hfake.root histograms.singletop.root histograms.ttbar.root histograms.ttgamma_dec.root histograms.ttgamma_prod.root histograms.ttv.root histograms.wgamma.root histograms.wjets.root histograms.Wt_inclusive.root histograms.Wty.root histograms.zgamma.root histograms.zjets.root
  popd || exit
  # lets add all regions data and MC; from this we will create the weights; the reason we do this because same weights should be applied to all the regions as we have only one particle level
  rm all_data.root all_mc.root
  hadd all_mc.root $path_unfolding_inputs/CR_sig/nominal_9999/histograms.all_MCs.root  $path_unfolding_inputs/CR_bkg/nominal_9999/histograms.all_MCs.root 
  hadd all_data.root $path_unfolding_inputs/CR_sig/nominal_9999/histograms.data.root  $path_unfolding_inputs/CR_bkg/nominal_9999/histograms.data.root 
  # ------------

  # ttgamma_prod NN06 CR_sig Y = 1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc $rootfile_signal_mc --rootfile_all_data all_data.root  --outfile $outfile --param_Y 1
  
  # ttgamma_prod NN06 CR_sig Y = -1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc $rootfile_signal_mc --rootfile_all_data all_data.root --outfile $outfile --param_Y -1

  # Bkg
  CR="CR_bkg"
  rootfile_all_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.all_MCs.root"
  rootfile_signal_mc="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine//$CR/nominal_9999/histograms.ttgamma_prod.root"
  #rootfile_signal_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_dec.root" # wanted to reweight dec
  rootfile_data="$path_unfolding_inputs/$CR/nominal_9999/histograms.data.root"
  outfile="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine//$CR/nominal_9999/ttgamma_prod"
  #outfile="$path_unfolding_inputs/$CR/nominal_9999/ttgamma_dec" # wanted to reweight dec

  # ttgamma_prod NN06 CR_sig Y = 1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc $rootfile_signal_mc --rootfile_all_data all_data.root --outfile $outfile --param_Y 1
  
  # ttgamma_prod NN06 CR_sig Y = -1
  ./generate_reweighted_samples.py --rootfile_all_mc all_mc.root --rootfile_signal_mc $rootfile_signal_mc --rootfile_all_data all_data.root --outfile $outfile --param_Y -1
#
}

# Generate linear reweighted smaples
function linear_reweight {
# ttgamma_prod NN06 CR_sig Y = 1
CR="CR_sig"
rootfile_mc="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine//$CR/nominal_9999/histograms.ttgamma_prod.root"
#rootfile_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_dec.root"
outfile="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine//$CR/nominal_9999/histograms.ttgamma_prod"
#outfile="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_dec"
./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y 1
#
## ttgamma_prod NN06 CR_sig Y = -1
./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y -1
#
CR="CR_bkg"
rootfile_mc="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine//$CR/nominal_9999/histograms.ttgamma_prod.root"
#rootfile_mc="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_dec.root"
outfile="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine//$CR/nominal_9999/histograms.ttgamma_prod"
#outfile="$path_unfolding_inputs/$CR/nominal_9999/histograms.ttgamma_dec"

## ttgamma_prod NN06 CR_bkg Y = 1
./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y 1
#
## ttgamma_prod NN06 CR_bkg Y = -1
./generate_linear_reweighted_samples.py --rootfile_mc $rootfile_mc --outfile $outfile --param_Y -1
}

# Generate linear reweighted smaples
#linear_reweight
non_linear_reweight
