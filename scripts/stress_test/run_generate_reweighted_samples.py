from generate_reweighted_samples import *


def generate_non_linear_reweighted_samples():
  rootfile_all_mc="/eos/home-b/bmondal/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v7/NN06/CR_sig/nominal_9999/histograms.all_MCs.root"
  rootfile_signal_mc="/eos/home-b/bmondal/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v7/NN06/CR_sig/nominal_9999/histograms.ttgamma_prod.root"
  rootfile_data="/eos/home-b/bmondal/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v7/NN06/CR_sig/nominal_9999/histograms.data.root"
  outfile="/eos/home-b/bmondal/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v7/NN06/CR_sig/nominal_9999/ttgamma_prod"



if __name__ == "__main__":
  generate_non_linear_reweighted_samples()