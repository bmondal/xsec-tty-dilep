#run_folder="../../abs-xsec-with-trexfiles-local/v12/v12/syst-all/"
run_folder="../../abs-xsec-with-trexfiles-local/v12/v15/syst-all/"
#syst
function run_syst {
  #run_folder="../../abs-xsec-with-trexfiles-local/v12/v15/syst-all/"
  run_folder="/afs/cern.ch/work/b/bmondal/ttgamma-analysis/DiffXsec_dilep/Unfolding_139fb/xsec-tty-dilep/abs-xsec-with-trexfiles-afs/v12/v15/syst-all"
  trex_config_path="../generate_config_file/syst-all"
  pushd ../generate_config_file/
  ./run_generate_syst.sh
  popd
  ./submit_ranking_condor.py  --config-file $trex_config_path/tty2l_pt_all_syst.config --run-folder ${run_folder} --submit
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_syst.config --run-folder ${run_folder}
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_syst.config --run-folder ${run_folder}
}

run_syst
