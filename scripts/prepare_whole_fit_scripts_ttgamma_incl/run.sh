run_folder="../../abs-xsec-with-trexfiles-local/v12/v12/syst-all/"
#run_folder="../../abs-xsec-with-trexfiles/v12/v12/syst-all/"
#syst
function run_syst {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v31/syst-all"
  mkdir -p $run_folder
  trex_folder="../../../xsec-tty-ljet/TRExFitter/"
  trex_config_file_path="../generate_config_file_ttgamma_incl/syst-all"
  pushd ../generate_config_file_ttgamma_incl/
  rm -rf *.config
  source run_generate_syst.sh && fit_asimov
  popd
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_pt_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_eta_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dr_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dr1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dr2_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_drphb_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_drlj_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dEtall_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dPhill_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_ptj1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_ptll_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
}
function run_syst_real_data_mu_blinded() {
  #run_folder="../../abs-xsec-with-trexfiles-local/v12/v37_splitted_syst_last_bin_normbin/syst-all-fit-data-mu-blinded/"
  #run_folder="../../abs-xsec-with-trexfiles-local/v12/v37_splitted_syst/syst-all-fit-data-mu-blinded/"
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v35_splitted_syst/syst-all-fit-data-mu-blinded/"
  mkdir -p $run_folder
  trex_folder="../../../xsec-tty-ljet/TRExFitter/"
  trex_config_file_path="../generate_config_file_ttgamma_incl/syst-all-fit-data-mu-blinded/"
  pushd ../generate_config_file_ttgamma_incl/
  rm -rf *.config
  source run_generate_syst.sh && fit_data_mu_blinded
  popd
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_pt_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_eta_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dr_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dr1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dr2_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_drphb_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_drlj_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dEtall_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_dPhill_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_ptj1_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file $trex_config_file_path/tty2l_ptll_all_syst.config --run-folder ${run_folder} --trex-folder ${trex_folder}
}

#stat
function run_stat {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v13/stat-all/"
  trex_folder="../../../xsec-tty-ljet/TRExFitter/"
  pushd ../generate_config_file/
  source run_generate_stat.sh && generate_config_stat_all_var
  popd
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_stat.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # reweighted ones
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/
  #./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/
}
function run_stat_reweighted {
  run_folder="../../abs-xsec-with-trexfiles-local/v12/v13/stat-all/"
  trex_folder="../../TRExFitter/"
  pushd ../generate_config_file/
  source run_generate_stat.sh && generate_config_reweighted_all_var
  popd
  ### >>>>>>>>>>>>>>>>>>>>> pt <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/  --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_pt_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/  --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> eta <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_eta_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> dr <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> dr1 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr1_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> dr2 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dr2_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> dEtall <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dEtall_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> dPhill <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_dPhill_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> Ptll <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptll_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> drphb <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drphb_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> drlj <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_drlj_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ### >>>>>>>>>>>>>>>>>>>>> pt j1 <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  # reweighted ones
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_stat_linear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_stat_linear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  # nonlinear
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_stat_nonlinear_reweighted_y1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}
  ./submit_fit_condor.py  --config-file ../generate_config_file/tty2l_ptj1_all_stat_nonlinear_reweighted_y-1.config --run-folder ${run_folder}/ --trex-folder ${trex_folder}

}

#run_syst
run_syst_real_data_mu_blinded
#run_stat
#run_stat_reweighted
