rm tty2l_pt_all_syst
rm tty2l_eta_all_syst
rm tty2l_dr_all_syst
rm tty2l_dr1_all_syst
rm tty2l_dr2_all_syst
rm tty2l_dEtall_all_syst
rm tty2l_dPhill_all_syst
rm tty2l_ptll_all_syst
rm tty2l_drphb_all_syst
rm tty2l_drlj_all_syst
rm tty2l_ptj1_all_syst





ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_pt_all_syst tty2l_pt_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_eta_all_syst tty2l_eta_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_dr_all_syst tty2l_dr_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_dr1_all_syst tty2l_dr1_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_dr2_all_syst tty2l_dr2_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_dEtall_all_syst tty2l_dEtall_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_dPhill_all_syst tty2l_dPhill_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_ptll_all_syst tty2l_ptll_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_drphb_all_syst tty2l_drphb_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_drlj_all_syst tty2l_drlj_all_syst
ln -s ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/tty2l_ptj1_all_syst tty2l_ptj1_all_syst

cp ../../../abs-xsec-with-trexfiles-local/v12/v34/syst-all-fit-data-mu-blinded/*.config .
