#!/usr/bin/env python
from getInfoFromRootFile import *

input_file_for_reading_tree = "/eos/user/a/ankirchh/ttgamma-analysis/mini-ntuples/fit_v12_dilep_injected/mc16e_TOPQ1_singletop_tchan410659.1.root"
obj = ReadFromRootFile(input_file_for_reading_tree)
f = open("np_title_dic.py","w")
f.write("#!/usr/bin/env python \n")
f.write("np_dict = { \n")
for tree in obj.get_list_of_trees():
  f.write("  \"{0}\" : \"{1}\"\n".format(tree, tree))
f.write("} \n")
f.close()