#!/usr/bin/env bash
# This script is for generating file and copyting all the config file to a appropriate folder for running unfolding

function fit_asimov() {
  save_folder="syst-all"
  mkdir -p $save_folder
  python GenerateUnfoldingTrexConfig_pt.py --filename $save_folder/tty2l_pt_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_eta.py --filename $save_folder/tty2l_eta_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_dr.py --filename $save_folder/tty2l_dr_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_dr1.py --filename $save_folder/tty2l_dr1_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_dr2.py --filename $save_folder/tty2l_dr2_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_dPhill.py --filename $save_folder/tty2l_dPhill_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_dEtall.py --filename $save_folder/tty2l_dEtall_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_ptll.py --filename $save_folder/tty2l_ptll_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_ptj1.py --filename $save_folder/tty2l_ptj1_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_drphb.py --filename $save_folder/tty2l_drphb_all_syst.config  --write-syst
  python GenerateUnfoldingTrexConfig_drlj.py --filename $save_folder/tty2l_drlj_all_syst.config  --write-syst
}

function fit_data_mu_blinded() {
  save_folder="./syst-all-fit-data-mu-blinded"
  mkdir -p $save_folder
  python GenerateUnfoldingTrexConfig_pt.py --filename $save_folder/tty2l_pt_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_eta.py --filename $save_folder/tty2l_eta_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_dr.py --filename $save_folder/tty2l_dr_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_dr1.py --filename $save_folder/tty2l_dr1_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_dr2.py --filename $save_folder/tty2l_dr2_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_dPhill.py --filename $save_folder/tty2l_dPhill_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_dEtall.py --filename $save_folder/tty2l_dEtall_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_ptll.py --filename $save_folder/tty2l_ptll_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_ptj1.py --filename $save_folder/tty2l_ptj1_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_drphb.py --filename $save_folder/tty2l_drphb_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
  python GenerateUnfoldingTrexConfig_drlj.py --filename $save_folder/tty2l_drlj_all_syst.config  --write-syst --write-data --use-real-data --fit-blind
}
