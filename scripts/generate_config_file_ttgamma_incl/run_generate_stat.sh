#!/usr/bin/env bash
# This script is for generating file and copyting all the config file to a appropriate folder for running unfolding

function generate_config_stat_all_var {
  python GenerateUnfoldingTrexConfig_pt.py --filename tty2l_pt_all_stat.config  --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_eta.py --filename tty2l_eta_all_stat.config  --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_dr.py --filename tty2l_dr_all_stat.config   --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_dr1.py --filename tty2l_dr1_all_stat.config --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_dr2.py --filename tty2l_dr2_all_stat.config --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_dPhill.py --filename tty2l_dPhill_all_stat.config  --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_dEtall.py --filename tty2l_dEtall_all_stat.config --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_ptll.py --filename tty2l_ptll_all_stat.config   --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_ptj1.py --filename tty2l_ptj1_all_stat.config  --write-data --fit-blind 
  #python GenerateUnfoldingTrexConfig_drphb.py --filename tty2l_drphb_all_stat.config   --write-data --fit-blind
  #python GenerateUnfoldingTrexConfig_drlj.py --filename tty2l_drlj_all_stat.config   --write-data --fit-blind
}


# generate reweighted configs
function generate_config_reweighted {
var_name=$1
#linear
python GenerateUnfoldingTrexConfig_${var_name}.py --filename tty2l_${var_name}_all_stat_linear_reweighted_y1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name histograms.ttgamma_prod.linear_reweighted_y1.0
python GenerateUnfoldingTrexConfig_${var_name}.py --filename tty2l_${var_name}_all_stat_linear_reweighted_y-1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name histograms.ttgamma_prod.linear_reweighted_y-1.0
# nonlinear
python GenerateUnfoldingTrexConfig_${var_name}.py --filename tty2l_${var_name}_all_stat_nonlinear_reweighted_y1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name  ttgamma_prod_reweighted_y1.0 
python GenerateUnfoldingTrexConfig_${var_name}.py --filename tty2l_${var_name}_all_stat_nonlinear_reweighted_y-1.config  --write-data --fit-blind --use_reweighted_pseudodata --reweighted_data_name ttgamma_prod_reweighted_y-1.0 
}

function generate_config_reweighted_all_var {
  generate_config_reweighted "pt"
  generate_config_reweighted "eta"
  generate_config_reweighted "dr"
  generate_config_reweighted "dr1"
  generate_config_reweighted "dr2"
  generate_config_reweighted "dEtall"
  generate_config_reweighted "dPhill"
  generate_config_reweighted "ptll"
  generate_config_reweighted "drphb"
  generate_config_reweighted "ptj1"
  generate_config_reweighted "drlj"
}
