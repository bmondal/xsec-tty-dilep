#!/usr/bin/env python3
import subprocess
import os
import threading

new_binning = "nominal_from_fine"
#new_binning = "binning_1"
#new_binning = "binning_1"
#path_to_new_binning="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v12_v5/NN06/"
#path_to_new_binning="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11_v11/NN06/"
#path_to_new_binning="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11_v11_v1/NN06/"
#path_to_new_binning="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/2ndLepTest/NN06/"
path_to_new_binning="/home/bm863639/work/ttgamma-analysis/analysis_framework/binning_optimization_dilep/run_sig_06/test/nominal_9999/"
n_cpus = 55

def rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others):
    #path = os.path.expanduser(f"~/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v14/NN06/{CR}/{subfolder}")
    path = os.path.expanduser(f"{path_to_new_binning}/{CR}/{subfolder}")
    subprocess.run(["./Rebin_all_samples.py", "--top_dir", path, "--binning_name", binning_name, "--new_binning_name", optimized_binning, "--optimized_binning", optimized_binning, "--max_rel_err_val_changer_pt", str(changer_pt), "--max_rel_err_val_changer_others", str(changer_others), "--n_cpus", str(n_cpus)])

def test():
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    path = "tmp/"
    subprocess.run(["./Rebin_all_samples.py", "--top_dir", path, "--binning_name", binning_name, "--new_binning_name", optimized_binning, "--optimized_binning", optimized_binning, "--max_rel_err_val_changer_pt", str(changer_pt), "--max_rel_err_val_changer_others", str(changer_others)])



def rebin_sig(subfolder):
    CR = "CR_sig"
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others)

def rebin_bkg(subfolder):
    CR = "CR_bkg"
    binning_name = "Fine_Binning_1"
    optimized_binning = new_binning
    changer_pt = 0.0
    changer_others = 0.0
    rebin(CR, subfolder, binning_name, optimized_binning, changer_pt, changer_others)

def run_sig_over_specific_dirs(txt_file): # run over specific dirs provided in txt file
    file_ = open(txt_file,"r")
    for line in file_.readlines():
      if line.strip(): # check if line is empty
        #if "nominal_9999" in line: continue # ignoring nominal_999 because this folder contains more files
        rebin_sig("{}".format(line.strip()))

def run_bkg_over_specific_dirs(txt_file): # run over specific dirs provided in txt file
    file_ = open(txt_file,"r")
    for line in file_.readlines():
      if line.strip():
        #if "nominal_9999" in line: continue # ignoring nominal_999 because this folder contains more files
        rebin_bkg(line.strip())

if __name__=="__main__":
  #rebin_sig("/") # if you don't wanna run over nominal just use "/"
  #rebin_bkg("/")
  #test()
  run_over_specific_folders = False 
  run_over_all_folders = True 

  if run_over_specific_folders:
    folders_to_process_sig = "maybe_corrupted_during_rebinning_CR_sig.txt"
    folders_to_process_bkg = "maybe_corrupted_during_rebinning_CR_bkg.txt"
    ## Create two threads
    #thread1 = threading.Thread(target=run_sig_over_specific_dirs, args=(folders_to_process_sig,))
    #thread2 = threading.Thread(target=run_bkg_over_specific_dirs, args=(folders_to_process_bkg,))
    run_sig_over_specific_dirs(folders_to_process_sig)
    run_bkg_over_specific_dirs(folders_to_process_bkg)

  if run_over_all_folders:
    rebin_sig("/")
    rebin_bkg("/")
    ## Create two threads
    thread1 = threading.Thread(target=rebin_sig, args=("/"))
    thread2 = threading.Thread(target=rebin_bkg, args=("/"))

    # Start the threads
    #thread1.start()
    #thread2.start()

