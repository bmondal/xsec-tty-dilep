import ROOT

root_file = "Ranking_Unfolding_Bin_001_mu.root"

# Open the ROOT file
f = ROOT.TFile.Open(root_file)

# Get the canvas from the file
canvas = f.Get("c")

# Get the pad inside the canvas
pad = canvas.GetPad(0)

# Increase the pad size to prevent y-axis labels from being cut
pad.SetPad(0,0.2,1,1)

# Get the list of objects in the canvas
list_objects = canvas.GetListOfPrimitives()

# Iterate over the objects in the list
for i in range(list_objects.GetSize()):
    obj = list_objects.At(i)
    # Check if the object is a TText (which is used for axis labels and titles)
    if obj.InheritsFrom("TText"):
        #store the text size
        original_size = obj.GetTextSize()

# Save the canvas as a PDF
root_file_name = root_file
pdf_file_name =  root_file_name.replace(".root",".pdf")
canvas.SaveAs(pdf_file_name)

#Iterate over the objects in the list again
for i in range(list_objects.GetSize()):
    obj = list_objects.At(i)
    # Check if the object is a TText (which is used for axis labels and titles)
    if obj.InheritsFrom("TText"):
        #set the text size to original size
        obj.SetTextSize(original_size)

# Close the file
f.Close()

