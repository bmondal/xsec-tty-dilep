# Open the text file in read mode
variables = ["log_pt","log_eta","log_dr","log_dr1","log_dr2","log_dEtall","log_dPhill","log_ptll","log_drphb","log_drlj","log_ptj1"]

abs_norm = ["v34","v35"]

print("---------------------------------------------------------------------------\n")
print("| Variable |\t GOF (tty_prod) | GOF(tty_total) |\n")
print("---------------------------------------------------------------------------\n")
for var in variables:
  results= []
  for region in abs_norm:
    filename = "/home/bm863639/Storage/HEP/ttgamma/DiffXSec/Unfolding/xsec-tty-dilep/abs-xsec-with-trexfiles-local/v12/{}/syst-all-fit-data-mu-blinded/{}.txt".format(region,var)
    with open(filename, 'r') as file:
        lines = file.readlines()
    
    # List to store extracted values
    
    # Iterate over the lines with index
    for i, line in enumerate(lines):
        clean_line = line.replace("\x1b[0m", "")
        # If the line contains the search string
        if "GOODNESS OF FIT EVALUATION" in line:
            # Extract chi2, ndf, and prob values from the next three lines
            prob = round(float((lines[i + 6].replace("\x1b[0m", "").split(":")[5].split("=")[1]).strip()),2)
            #prob = round(float((lines[i + 3].replace("\x1b[0m", "").split(":")[6].split("^")[0]).strip()),2)
    results.append(prob)

  # Print the results
  print("| {} |\t {} |\t {}\n".format(var, results[0], results[1]) )

