import ROOT

def get_histogram(file_path, hist_name):
    file = ROOT.TFile.Open(file_path)
    
    if not file or file.IsZombie():
        raise Exception(f"Failed to open file: {file_path}")
    
    hist = file.Get(hist_name)
    
    if not hist:
        file.Close()
        raise Exception(f"Histogram '{hist_name}' not found in file: {file_path}")
    
    return hist, file

def create_ratio_plot(hist1, hist2, title="Ratio"):
    ratio_hist = hist1.Clone("ratio_hist")
    ratio_hist.Divide(hist2)
    ratio_hist.SetTitle(title)
    ratio_hist.SetMinimum(0.5)  # Define y-axis range for the ratio plot
    ratio_hist.SetMaximum(1.5)
    return ratio_hist

def main():
    ROOT.gROOT.SetBatch(True)
    
    #hist_name = "Reco/hist_reco_dPhill_full_weighted"
    hist_name = "Reco/hist_reco_dEtall_full_weighted"
    
    hist_tty_dec_SR, file_tty_dec_SR = get_histogram("CR_sig/histograms.ttgamma_dec.root", hist_name)
    hist_tty_dec_CR, file_tty_dec_CR = get_histogram("CR_bkg/histograms.ttgamma_dec.root", hist_name)
    #hist_zgamma_SR, file_zgamma_SR = get_histogram("CR_sig/histograms.zgamma.root", hist_name)
    #hist_zgamma_CR, file_zgamma_CR = get_histogram("CR_bkg/histograms.zgamma.root", hist_name)
    hist_zgamma_SR, file_zgamma_SR = get_histogram("CR_sig/histograms.ttgamma_prod.root", hist_name)
    hist_zgamma_CR, file_zgamma_CR = get_histogram("CR_bkg/histograms.ttgamma_prod.root", hist_name)

    
    hist_tty_dec_SR.Scale(1./hist_tty_dec_SR.Integral())
    hist_tty_dec_CR.Scale(1./hist_tty_dec_CR.Integral())
    hist_zgamma_SR.Scale(1./hist_zgamma_SR.Integral())
    hist_zgamma_CR.Scale(1./hist_zgamma_CR.Integral())
    hist_tty_dec_SR.GetYaxis().SetRangeUser(0.0, 1.0)
    hist_tty_dec_CR.GetYaxis().SetRangeUser(0.0, 1.0)
    hist_zgamma_SR.GetYaxis().SetRangeUser(0.0, 1.0)
    hist_zgamma_CR.GetYaxis().SetRangeUser(0.0, 1.0)


    # Create the ratio plots
    ratio_SR = create_ratio_plot(hist_tty_dec_SR, hist_zgamma_SR, "tty_dec/zy")
    ratio_CR = create_ratio_plot(hist_tty_dec_CR, hist_zgamma_CR, "tty_dec/zy")

    # Drawing SR plots
    canvas_SR = ROOT.TCanvas("canvas_SR", "", 800, 800)
    pad1_SR = ROOT.TPad("pad1_SR", "", 0, 0.3, 1, 1.0)
    pad1_SR.SetBottomMargin(0)
    pad1_SR.Draw()
    pad1_SR.cd()
    hist_tty_dec_SR.Draw("hist")
    hist_zgamma_SR.SetLineColor(ROOT.kRed)
    hist_zgamma_SR.Draw("same hist e")
    canvas_SR.cd()
    pad2_SR = ROOT.TPad("pad2_SR", "", 0, 0.05, 1, 0.3)
    pad2_SR.SetTopMargin(0)
    pad2_SR.SetBottomMargin(0.2)
    pad2_SR.Draw()
    pad2_SR.cd()
    ratio_SR.GetYaxis().SetRangeUser(-0.5,3.0)
    ratio_SR.Draw("ep")
    canvas_SR.SaveAs("tty_dec_tty_prod_normalized_comp_SR_with_ratio.pdf")

    # Drawing CR plots
    canvas_CR = ROOT.TCanvas("canvas_CR", "", 800, 800)
    pad1_CR = ROOT.TPad("pad1_CR", "", 0, 0.3, 1, 1.0)
    pad1_CR.SetBottomMargin(0)
    pad1_CR.Draw()
    pad1_CR.cd()
    hist_tty_dec_CR.Draw("hist")
    hist_zgamma_CR.SetLineColor(ROOT.kRed)
    hist_zgamma_CR.Draw("same hist e")
    canvas_CR.cd()
    pad2_CR = ROOT.TPad("pad2_CR", "", 0, 0.05, 1, 0.3)
    pad2_CR.SetTopMargin(0)
    pad2_CR.SetBottomMargin(0.2)
    pad2_CR.Draw()
    pad2_CR.cd()
    ratio_CR.GetYaxis().SetRangeUser(-0.5,3.0)
    ratio_CR.Draw("ep")
    #canvas_CR.SaveAs("tty_dec_zy_normalized_comp_CR_with_ratio.pdf")
    canvas_CR.SaveAs("tty_dec_tty_prod_normalized_comp_CR_with_ratio.pdf")

    # Close files
    file_tty_dec_SR.Close()
    file_tty_dec_CR.Close()
    file_zgamma_SR.Close()
    file_zgamma_CR.Close()

if __name__=="__main__":
    try:
        main()
    except Exception as e:
        print(f"Error: {e}")

