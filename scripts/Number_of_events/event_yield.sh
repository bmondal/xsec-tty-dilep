#!/usr/bin/env bash
out_file_efake="efake_yield_v250823.txt"
out_file_hfake="hfake_yield_v250823.txt"
function run_efake() {
  ## tty_prod
  echo -e "event yield"
  echo -e "------------------------------------------"
  echo "efake_ttgamma_NLO_prod"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_tty_NLO_prod.root
  echo "efake_ttgamma_LO_dec"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_tty_LO_dec.root
  echo "efake_Wgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wgamma.root
  echo "efake_Zgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_zgamma.root
  echo "efake_Wjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wjets.root
  echo "efake_Zjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_zjets.root
  echo "efake_Wtgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wty.root
  echo "efake_Wt_inclusive"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_wt_inclusive.root
  echo "efake_ttbar"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_ttbar.root
  echo "efake_singletop"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_singletop.root
  echo "efake_ttV"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_ttv.root
  echo "efake_diboson"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.efake_diboson.root
}

function run_hfake() {
  ## tty_prod
  echo -e "event yield"
  echo -e "------------------------------------------"
  echo "hfake_ttgamma_NLO_prod"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_tty_NLO_prod.root
  echo "hfake_ttgamma_LO_dec"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_tty_LO_dec.root
  echo "hfake_Wgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wgamma.root
  echo "hfake_Zgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_zgamma.root
  echo "hfake_Wjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wjets.root
  echo "hfake_Zjets"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_zjets.root
  echo "hfake_Wtgamma"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wty.root
  echo "hfake_Wt_inclusive"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_wt_inclusive.root
  echo "hfake_ttbar"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_ttbar.root
  echo "hfake_singletop"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_singletop.root
  echo "hfake_ttV"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_ttv.root
  echo "hfake_diboson"
  python event_yield.py --rootfilepath $1/  --rootfilename nominal_9999/histograms.hfake_diboson.root
}


# be mindful I am using local eos folder here
run_efake $HOME/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/ > $out_file_efake
#run_tty_dec "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/" >> $out_file_tty_dec

run_hfake $HOME/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/ > $out_file_hfake
#run_tty_dec "~/eos/physics_analysis/tty/ljet/Unfolding_samples/v12/Unfolding_inputs_v12_v11/" >> $out_file_tty_dec
