import os
import argparse

debug = False

def extract_data_from_file(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    # Extract control region names
    control_regions = [item.strip() for item in lines[0].split('|')[2:-1]]
    if debug: print(f"control regions: {control_regions}")

    # Extract process names and corresponding values
    processes = []
    values = []
    for row in lines[1:]:
        items = [item.strip() for item in row.split('|')]
        process_name = items[1]
        if debug: print(f"process names: {process_name}")
        try:
            process_values = [int(float(item.split(' pm ')[0])) for item in items[2:-1]]
            processes.append(process_name)
            values.append(process_values)
        except ValueError:
            continue

    return control_regions, processes, values

def main(base_directory):
    all_folders = [os.path.join(base_directory, d) for d in os.listdir(base_directory) if os.path.isdir(os.path.join(base_directory, d))]

    # Adjust file path to include the "Tables" subfolder
    reference_file_path = os.path.join(all_folders[0], 'Tables', 'Yields.txt')
    
    control_regions, processes, reference_values = extract_data_from_file(reference_file_path)

    # Print the reference values
    print("Reference values from:", all_folders[0])
    for process, vals in zip(processes, reference_values):
        print(f"{process}: {vals}")
    print("\n")

    # Now, compare with the other directories
    for folder in all_folders[1:]:
        file_path = os.path.join(folder, 'Tables', 'Yields.txt')
        _, _, current_values = extract_data_from_file(file_path)

        for process, ref_vals, curr_vals in zip(processes, reference_values, current_values):
            for ref_val, curr_val, region in zip(ref_vals, curr_vals, control_regions):
                if ref_val != curr_val:
                    print(f"Discrepancy in {folder} for process {process} in control region {region}. Expected: {ref_val}, Found: {curr_val}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract and print reference values from the Yields.txt in the first directory\'s Tables subfolder, then compare with other folders.')
    parser.add_argument('--base_directory', type=str, help='Path to the base directory containing folders with Tables subfolders and Yields.txt files', required=True)
    
    args = parser.parse_args()
    main(args.base_directory)

