#!/usr/bin/env python3
import ROOT

histo_path_cr_sig= "/Users/buddhadeb/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_sig/nominal_9999/"
histo_path_cr_bkg= "/Users/buddhadeb/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_bkg/nominal_9999/"

def make_tty_frac_template(root_file1, root_file2):  
  file_tty_prod = ROOT.TFile.Open(root_file1)
  file_tty_dec = ROOT.TFile.Open(root_file2)
  
  # Check that the histograms exist in the input files
  histo_tty_prod = file_tty_prod.Get("Reco/hist_reco_ph_pt_full_weighted")
  histo_tty_dec = file_tty_dec.Get("Reco/hist_reco_ph_pt_full_weighted")
  # Clone the input histogram and divide it
  histo_tty_prod_clone = histo_tty_prod.Clone() 
  histo_tty_prod_clone.Divide(histo_tty_dec)
  
  histo_tty_prod_clone.SetDirectory(0)
  # Close the input ROOT files
  return histo_tty_prod_clone
 
def make_tty_sum_template(root_file1, root_file2):  
  file_tty_prod = ROOT.TFile.Open(root_file1)
  file_tty_dec = ROOT.TFile.Open(root_file2)
  
  # Check that the histograms exist in the input files
  histo_tty_prod = file_tty_prod.Get("Reco/hist_reco_ph_pt_full_weighted")
  histo_tty_dec = file_tty_dec.Get("Reco/hist_reco_ph_pt_full_weighted")
  
  # Clone the input histogram and divide it
  histo_tty_prod_clone = histo_tty_prod.Clone() 
  histo_tty_prod_clone.Add(histo_tty_dec)
  
  histo_tty_prod_clone.SetDirectory(0)
  return histo_tty_prod_clone


def run_sig():
  root_file1 = histo_path_cr_sig+"histograms.ttgamma_prod.root"
  root_file2 = histo_path_cr_sig+"histograms.ttgamma_dec.root"
  # make fraction template
  histo_frac = make_tty_frac_template(root_file1, root_file2)
  histo_frac.SetName("hist_frac_tty_reco_ph_pt_full_weighted")
  histo_sum = make_tty_sum_template(root_file1, root_file2)
  histo_sum.SetName("hist_sum_tty_reco_ph_pt_full_weighted")

  if histo_frac is None or histo_sum is None:
    print("Error: histo_frac or histo_sum is None type")

  # write to a new file
  output_file_name = histo_path_cr_sig + "histograms.frac_tty_prod_dec.root"
  output_file = ROOT.TFile.Open(output_file_name, "RECREATE")
  histo_frac.Write()
  histo_sum.Write()
  output_file.Close()

def run_bkg():
  root_file1 = histo_path_cr_bkg+"histograms.ttgamma_prod.root"
  root_file2 = histo_path_cr_bkg+"histograms.ttgamma_dec.root"
  # make fraction template
  histo_frac = make_tty_frac_template(root_file1, root_file2)
  histo_frac.SetName("hist_frac_tty_reco_ph_pt_full_weighted")
  histo_sum = make_tty_sum_template(root_file1, root_file2)
  histo_sum.SetName("hist_sum_tty_reco_ph_pt_full_weighted")

  if histo_frac is None or histo_sum is None:
    print("Error: histo_frac or histo_sum is None type")

  # write to a new file
  output_file_name = histo_path_cr_bkg+ "histograms.frac_tty_prod_dec.root"
  output_file = ROOT.TFile.Open(output_file_name, "RECREATE")
  histo_frac.Write()
  histo_sum.Write()
  output_file.Close()



if __name__=="__main__":
  run_sig()
  run_bkg()

