#!/usr/bin/env python3



import os
import subprocess
from multiprocessing import Pool, cpu_count
from calculate_response_matrix import *

debug = False

def merge_samples(subdir):
  # create filename for folder1 from subdir
  folder1 = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_sig/{}".format(subdir)
  # create filename for folder2 from subdir
  folder2 = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_bkg/{}".format(subdir)
  # create filename for folder3 from subdir
  folder3 = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_ttgamma_inclusive/SR/{}".format(subdir)

  # Loop over the files in the subdirectory
  for filename in os.listdir(folder1):
    # create filename for folder1 from filename
    filename_1 = os.path.join(folder1, filename)
    # create filename for folder2 from filename
    filename_2 = os.path.join(folder2, filename)
    if debug: print("Debug:: {}/{}".format(subdir, filename))

    # create directory if not exists
    if not os.path.exists(folder3):
      os.makedirs(folder3)
      print("Directory {} created successfully \n".format(folder3))

    # Check if the file exists in folder2
    if os.path.exists(filename_2):
      # create filename for folder3 from filename
      filename_3 = os.path.join(folder3, filename)

      # Create the command to merge the root files using hadd
      hadd_command = "hadd -v 0 -f {0} {1} {2}".format(filename_3, filename_1, filename_2)
      if debug: print("Info:: running {}".format(hadd_command))

      # Execute the hadd command
      subprocess.call(hadd_command, shell=True)

    else:
      print("Error:: file path doesn't exist {}".format(filename_2))

  # display progress
  print("{} processed".format(subdir))


if __name__ == "__main__":
  submit_merging_part = False
  submit_response_matrix_part = True
  folder1 = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_sig/"
  folder3 = "/home/buddha/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_ttgamma_inclusive/SR/"
  binning_name = "."
  num_processes = cpu_count() - 2 

  # Get a list of the subdirectories to process
  subdirs = [d for d in os.listdir(folder1) if os.path.isdir(os.path.join(folder1, d))]

  if submit_merging_part:
    with Pool(processes=num_processes) as pool:
      pool.map(merge_samples, subdirs)

  if submit_response_matrix_part:
    calculate_and_save_response_matrices_over_main_dir(folder3,binning_name)

