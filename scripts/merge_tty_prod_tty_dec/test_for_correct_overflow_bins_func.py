from correct_overflow_bins import *

import ROOT
import random


def print_th2d_bin_contents(histogram):
    nx = histogram.GetNbinsX()+1 # to print also the overflow bin
    ny = histogram.GetNbinsY()+1
    for j in range(ny, 0, -1):
        row = []
        for i in range(1, nx+1):
            row.append(histogram.GetBinContent(i, j))
        print(*row)
    print()



# Create a test histogram
h = ROOT.TH2F("test_histogram", "Test Histogram", 5, 0, 5, 5, 0, 5)

# Fill the histogram with random values
for i in range(1,6):
    for j in range(1,6):
        h.SetBinContent(i, j, i + j)
        h.SetBinError(i, j, i * j)

# Fill the overflow bins with random values
h.SetBinContent(6, 1, 11)
h.SetBinContent(6, 2, 3)
h.SetBinContent(6, 5, 4)
h.SetBinError(6, 1, 5)
h.SetBinContent(1, 6, 12)
h.SetBinError(1, 6, 6)
h.SetBinContent(5, 6, 10)
h.SetBinContent(6, 6, 13)
h.SetBinError(6, 6, 7)

# print the histogram content before adding overflow
print("Before adding overflow to previos bin \n")
print_th2d_bin_contents(h)

# Call the add_overflow_to_previous function
h = add_overflow_to_previous(h)

print("After adding overflow to previos bin \n")
print_th2d_bin_contents(h)
