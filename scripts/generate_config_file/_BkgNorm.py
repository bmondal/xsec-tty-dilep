from np_dict_tty_ttZ_v1 import *
class BkgNorm:
  def write_bkg_norm_uncertainty(self):

    # prompt Wty normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("wty_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["wty_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["wty_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["wty_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["wty_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.wjets, {0}/histograms.Wt_inclusive , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.Wty_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.wjets, {0}/histograms.Wt_inclusive  , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.Wty_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt Wt normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("wt_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["wt_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["wt_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["wt_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["wt_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.wjets, {0}/histograms.Wty, {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.Wt_inclusive_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.wjets, {0}/histograms.Wty, {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.Wt_inclusive_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # single top (s channel)
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("singletop_schan_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["singletop_schan_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["singletop_schan_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["singletop_schan_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["singletop_schan_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.wjets, {0}/histograms.Wt_inclusive , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.Wty, {0}/histograms.singletop_tchan \n".format("nominal_9999","histograms.singletop_schan_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.wjets, {0}/histograms.Wt_inclusive  , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.Wty, {0}/histograms.singletop_tchan  \n".format("nominal_9999","histograms.singletop_schan_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: SR2 \n")

    # single top (t channel)
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("singletop_tchan_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["singletop_tchan_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["singletop_tchan_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["singletop_tchan_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["singletop_tchan_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.wjets, {0}/histograms.Wt_inclusive , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.Wty, {0}/histograms.singletop_schan \n".format("nominal_9999","histograms.singletop_tchan_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.wjets, {0}/histograms.Wt_inclusive  , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.Wty, {0}/histograms.singletop_schan \n".format("nominal_9999","histograms.singletop_tchan_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: SR1, SR2 \n")

    # prompt Wy normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("wy_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["wy_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["wy_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["wy_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["wy_norm"]["Title"])) # correlated
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.wgamma_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.wgamma_NormVar"))
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt Zy normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("zy_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["zy_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["zy_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["zy_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["zy_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.zgamma_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.zgamma_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40\n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt ttV normalization  
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttv_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttv_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["ttv_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["ttv_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["ttv_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.singletop  \n".format("nominal_9999","histograms.ttv_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.singletop  \n".format("nominal_9999","histograms.ttv_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt diboson normalization
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("diboson_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["diboson_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["diboson_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["diboson_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["diboson_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.diboson_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.diboson_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # prompt ttbar normalization
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format("ttbar_norm"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_norm"]["ttZ_np_name"])) # correlated
    self.file.write("  Category: {} \n".format(np_dict["ttbar_norm"]["Category"])) # correlated
    self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_norm"]["SubCategory"])) # correlated
    self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_norm"]["Title"])) # correlated
    self.file.write("  HistoFilesUp:  {0}/{1}_Up , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.ttbar_NormVar"))
    self.file.write("  HistoFilesDown:  {0}/{1}_Down , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.ttbar_NormVar"))
    self.file.write("  Symmetrisation: TWOSIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))

    # tty_dec normalization uncertainty
#    self.file.write(" \n")
#    self.file.write("Systematic: {} \n".format("tty_dec_norm"))
#    self.file.write("  NuisanceParameter: {} \n".format("tty_dec_norm")) # correlated
#    self.file.write("  Category: {} \n".format("tty_dec_norm")) # correlated
#    self.file.write("  SubCategory: {} \n".format("tty_dec_norm")) # correlated
#    self.file.write("  HistoFilesUp:  {0}/{1}_Up  \n".format("nominal_9999","histograms.ttgamma_dec_NormVar"))
#    self.file.write("  HistoFilesDown:  {0}/{1}_Down  \n".format("nominal_9999","histograms.ttgamma_dec_NormVar"))
#    self.file.write("  Symmetrisation: TWOSIDED\n")
#    self.file.write("  Title: \"{}\" \n".format("t#bar{t}#gamma\_dec Normalization"))
#    self.file.write("  Type: HISTO \n")
#    self.file.write("  Samples: prompty\n")
#    self.file.write("  Regions: {} \n".format(self.regions))
