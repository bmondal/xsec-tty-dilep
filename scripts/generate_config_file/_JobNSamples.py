from constants import *
from Config import *
job_name_unfolding_name_dict = {
    "tty2l_dEtall_all_syst":"tty_dEtall",
    "tty2l_dPhill_all_syst":"tty_dPhill",
    "tty2l_dr1_all_syst":"tty_drphl1",
    "tty2l_dr2_all_syst":"tty_drphl2",
    "tty2l_dr_all_syst":"tty_min_drphl",
    "tty2l_drlj_all_syst":"tty_drlj",
    "tty2l_drphb_all_syst":"tty_drphb",
    "tty2l_eta_all_syst":"tty_eta",
    "tty2l_pt_all_syst":"tty_pt",
    "tty2l_ptj1_all_syst":"tty_ptj1",
    "tty2l_ptll_all_syst":"tty_ptll",
    "tty2l_dEtall_all_stat":"tty_dEtall",
    "tty2l_dPhill_all_stat":"tty_dPhill",
    "tty2l_dr1_all_stat":"tty_drphl1",
    "tty2l_dr2_all_stat":"tty_drphl2",
    "tty2l_dr_all_stat":"tty_min_drphl",
    "tty2l_drlj_all_stat":"tty_drlj",
    "tty2l_drphb_all_stat":"tty_drphb",
    "tty2l_eta_all_stat":"tty_eta",
    "tty2l_pt_all_stat":"tty_pt",
    "tty2l_ptj1_all_stat":"tty_ptj1",
    "tty2l_ptll_all_stat":"tty_ptll"
}

class JobNSamples:
  def write_config(self):
    self.file = open(self.config, 'w')
    self.file.write("Job: \"{}\"\n".format(self.job_name))
    self.file.write("  AtlasLabel: Internal \n")
    self.file.write("  Label: \"Dilepton\" \n")
    self.file.write("  CmeLabel: \"13 TeV\" \n")
    self.file.write("  DebugLevel: 2 \n")
    self.file.write("  LabelX: 0.17 \n")
    self.file.write("  LegendNColumns: 2 \n")
    self.file.write("  LegendX1: 0.48 \n")
    self.file.write("  LegendX2: 0.92 \n")
    self.file.write("  LegendY: 0.87 \n")
    self.file.write("  Logo: TRUE \n")
    self.file.write("  LumiLabel: \"140 fb^{-1}\" \n")
    self.file.write("  PlotOptions: NOXERR \n")
    self.file.write("  HistoPath: {}\n".format(self.histo_path_SR1))
    self.file.write("  ImageFormat: pdf, root\n")
    self.file.write("  ReadFrom: HIST\n")
    self.file.write("  SystControlPlots: FALSE\n")
    self.file.write("  UseGammaPulls: TRUE \n")
    #self.file.write("  DoExtendedCovariances: TRUE \n")
    self.file.write("  CorrelationThreshold: 0.1\n")
    #self.file.write("  CorrectNormForNegativeIntegral: TRUE\n")
    self.file.write("  SystPruningNorm: 0.001 \n")
    self.file.write("  SystPruningShape: 0.001 \n")
    self.file.write("  MCstatThreshold: 0.001 \n")
    self.file.write("  PruningType: COMBINEDSIGNAL \n")
    self.file.write("  UnfoldingShowStatOnlyError: TRUE \n")
    self.file.write("  UnfoldingShowUncertaintyBreakdown: TRUE \n")
    self.file.write("  UnfoldingUncertaintyBreakdownTotal: TRUE \n")
    self.file.write("  UnfoldingShowUncertaintyBreakdown: TRUE \n")
    self.file.write("  UnfoldingUncertaintyBreakdownTotal: TRUE \n")

    self.file.write(" \n")
    self.file.write("Fit: \"myFit\" \n")
    if(self.fit_blind):
      self.file.write("  FitBlind: TRUE\n")
    else:
      self.file.write("  FitBlind: FALSE\n")
    self.file.write("  FitRegion: CRSR \n")
    self.file.write("  FitType: UNFOLDING \n")
    self.file.write("  GetGoodnessOfFit: TRUE\n")
    self.file.write("  StatOnlyFit: TRUE \n")
    # depending on number of mu, calculate the minos only for mus
    #self.file.write("  UseMinos: all")
    self.file.write("  UseMinos: ")
    for bin in range(0, self.number_of_reco_bins):
      if unfolding_name:
        self.file.write(" {0}_Bin_00{1}_mu,".format(job_name_unfolding_name_dict[self.job_name],(bin+1))) # +1 because it mus numbering starts from 1
      else:
        self.file.write(" {0}_Bin_00{1}_mu,".format("Unfolding",(bin+1))) # +1 because it mus numbering starts from 1

    self.file.write("{}".format("tty_dec-SigXsecOverSM"))
    self.file.write(" \n")
    #if (self.use_data and self.use_real_data): # Hide mu if using real data
      #self.file.write("  BlindedParameters: ")
      #for bin in range(0, self.number_of_reco_bins):
        #self.file.write(" {0}_Bin_00{1}_mu,".format(job_name_unfolding_name_dict[self.job_name],(bin + 1)))  # +1 because it mus numbering starts from 1

      #self.file.write("{}".format("tty_dec-SigXsecOverSM"))
    #self.file.write(" \n")
    self.file.write("  NumCPU: 5 \n")

    self.file.write(" \n")
    self.file.write("NormFactor: tty_dec-SigXsecOverSM \n")
    self.file.write("  Max: 1.5 \n")
    self.file.write("  Min: 0.5 \n")
    self.file.write("  Nominal: 1 \n")
    self.file.write("  Title: \"Norm factor for tty dec\" \n")
    self.file.write("  Samples: \"tty_dec\" \n")
    self.file.write(" \n")

    self.file.write(" \n")
    if unfolding_name:
      self.file.write("Unfolding: \"{0}\" \n".format(job_name_unfolding_name_dict[self.job_name]))
    else:
      self.file.write("Unfolding: \"{0}\" \n".format("Unfolding"))
    self.file.write("  MatrixOrientation: TRUTHONVERTICAL \n")
    self.file.write("  NumberOfTruthBins: {} \n".format(self.number_of_reco_bins))
    self.file.write("  NominalTruthSample: \"pythia\" \n")
    self.file.write("  DivideByBinWidth: TRUE \n")
    self.file.write("  DivideByLumi: {} \n".format(LUMINOSITY))
    if(normalized_xsec_unfolding):
      self.file.write("  UnfoldNormXSec: TRUE \n")
      if (normalized_bin_n_minus_1):
        self.file.write("  UnfoldNormXSecBinN: {}\n".format(self.number_of_reco_bins - 1 ))
    self.file.write("  LogX: FALSE \n")
    self.file.write("  LogY: FALSE \n")
    self.file.write("  TitleX: \"{}\" \n".format(self.xaxisTitle))
    self.file.write("  TitleY: \"{}\" \n".format(self.yaxisTitle))
    self.file.write(" \n")
    #Truth pythia8
    self.file.write("TruthSample: \"pythia\" \n")
    self.file.write("  TruthDistributionPath: {} \n".format(self.truth_distribution_path))
    self.file.write("  TruthDistributionFile: {} \n".format(self.truth_distribution_file))
    self.file.write("  TruthDistributionName: {} \n".format(self.truth_distribution_name))
    self.file.write("  LineColor: 632 \n")
    self.file.write("  Title: \"aMC@NLO+P8\" \n")
    self.file.write(" \n")
    #Truth H7 
    self.file.write("TruthSample: \"herwig\" \n")
    self.file.write("  TruthDistributionPath: {} \n".format(self.truth_distribution_path))
    self.file.write("  TruthDistributionFile: {} \n".format(self.truth_distribution_file_H7))
    self.file.write("  TruthDistributionName: {} \n".format(self.truth_distribution_name))
    self.file.write("  LineColor: 600 \n")
    self.file.write("  Title: \"aMC@NLO+H7\" \n")
    self.file.write(" \n")

    #SR1
    self.file.write("Region: \"SR1\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR1))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  HistoPath: {} \n".format(self.histo_path_SR1))
    self.file.write("  HistoFile: {} \n".format(self.histo_file))
    self.file.write("  HistoName: {} \n".format(self.histo_name))
    self.file.write("  Label: \"O_{NN} #geq 0.6\" \n")
    self.file.write("  NumberOfRecoBins: {} \n".format(self.number_of_reco_bins))
    #self.file.write("  DataType: ASIMOV \n")
    self.file.write("  DataType: DATA \n")
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(self.variable_title))
    self.file.write(" \n")
    #SR2
    self.file.write("Region: \"SR2\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR2))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  HistoPath: {}\n".format(self.histo_path_SR2))
    self.file.write("  HistoFile: {}\n".format(self.histo_file))
    self.file.write("  HistoName: {} \n".format(self.histo_name))
    self.file.write("  Label: \"O_{NN} < 0.6\" \n")
    self.file.write("  NumberOfRecoBins: {} \n".format(self.number_of_reco_bins))
    #self.file.write("  DataType: ASIMOV\n")
    self.file.write("  DataType: DATA \n")
    self.file.write("  Type: SIGNAL \n")
    self.file.write("  VariableTitle: \"{}\" \n".format(self.variable_title))
    self.file.write(" \n")

    # Reference sample
    if(self.write_ttbar_af2):
      self.file.write(" \n")
      self.file.write("Sample: \"ttbar_AFII\" \n")
      self.file.write("  FillColorRGB: 255,255,51 \n")
      #self.file.write("  HistoFile: \"histograms.promptY_ttbar_AFII\" \n")
      self.file.write("  HistoFiles:  {0}/histograms.Wty , {0}/histograms.Wt_inclusive, {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/{1} , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.promptY_ttbar_AFII"))
      self.file.write("  LineColor: 31 \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Title: \"Prompt #gamma bkg\" \n")
      self.file.write("  Type: GHOST \n")
      self.file.write("  UseMCstat: TRUE \n")
      self.file.write(" \n")

    if(self.write_pdf_ref_sample):
      # tty_prod SR1
      self.file.write(" \n")
      self.file.write("UnfoldingSample: \"tty_prod_PDF4LHC_0\" \n")
      self.file.write("  ResponseMatrixFile: {}/{} \n".format("nominal_9999","histograms.tty_prod_PDF4LHC_0"))
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Type: GHOST \n")
      self.file.write(" \n")
      # tty_dec
      self.file.write("Sample: \"tty_dec_PDF4LHC_0\" \n")
      self.file.write("  HistoFiles: {}/{} \n".format("nominal_9999","histograms.tty_dec_PDF4LHC_0"))
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Type: GHOST \n")
      self.file.write("  UseMCstat: TRUE \n")
      self.file.write(" \n")


    # tty prod
    self.file.write("UnfoldingSample: \"tty_prod\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR1))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  FillColor: 2\n")
    self.file.write("  Regions: SR1 \n")
    self.file.write("  Title: \"t#bar{t}#gamma production\"\n")
    self.file.write(" \n")
    # multiple unfolding samples
    self.file.write("UnfoldingSample: \"tty_prod\" \n")
    self.file.write("  ResponseMatrixPath: {} \n".format(self.response_matrix_path_SR2))
    self.file.write("  ResponseMatrixFile: {} \n".format(self.response_matrix_file))
    self.file.write("  ResponseMatrixName: {} \n".format(self.response_matrix_name))
    self.file.write("  FillColor: 2\n")
    self.file.write("  Regions: SR2 \n")
    self.file.write("  Title: \"t#bar{t}#gamma production\"\n")
    self.file.write(" \n")

#    self.file.write("UnfoldingSample: \"tty_nominal\" \n")
#    self.file.write("  ResponseMatrixFile: {} \n".format(response_matrix_file))
#    self.file.write("  FillColor: 8 \n")
#    self.file.write("  Regions: SR \n")
#    self.file.write("  Title: \"tty_NLO_nominal\" \n")
    if self.use_data:
      if self.use_real_data:
        self.file.write(" \n")
        self.file.write("Sample: \"data\" \n")
        self.file.write("  FillColor: 9 \n")
        self.file.write("  HistoFile:  {}/histograms.data  \n".format("nominal_9999"))
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  Title: \"Data\" \n")
        self.file.write("  Type: data \n")
        self.file.write(" \n")
      elif self.use_reweighted_pseudodata:
        self.file.write(" \n")
        self.file.write("Sample: \"data\" \n")
        self.file.write("  FillColor: 9 \n")
        self.file.write("  HistoFiles:  {0}/histograms.efake, {0}/histograms.hfake, {0}/{1} , {0}/histograms.ttgamma_dec , {0}/histograms.Wty, {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999",self.reweighted_data_name))
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  Title: \"Data\" \n")
        self.file.write("  Type: data \n")
        self.file.write(" \n")
      else:
        self.file.write(" \n")
        self.file.write("Sample: \"data\" \n")
        self.file.write("  FillColor: 9 \n")
        self.file.write("  HistoFiles:  {0}/histograms.efake, {0}/histograms.hfake, {0}/histograms.ttgamma_prod , {0}/histograms.ttgamma_dec , {0}/histograms.Wty,{0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999"))
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  Title: \"Data\" \n")
        self.file.write("  Type: data \n")
        self.file.write(" \n")

    # tty dec template
    self.file.write("Sample: \"tty_dec\" \n")
    self.file.write("  FillColor: 3 \n")
    self.file.write("  HistoFile:  {0}/histograms.ttgamma_dec  \n".format("nominal_9999"))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Title: \"t#bar{t}#gamma decay\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")

    #hfake template
    self.file.write("Sample: \"hfake\" \n")
    self.file.write("  FillColor: 6 \n")
    self.file.write("  HistoFile:  {0}/histograms.hfake  \n".format("nominal_9999"))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Title: \"h-fake #gamma\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")

    # efake template
    self.file.write("Sample: \"efake\" \n")
    self.file.write("  FillColor: 7 \n")
    self.file.write("  HistoFile:  {0}/histograms.efake  \n".format("nominal_9999"))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Title: \"e-fake #gamma\"\n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")

    # prompty template
    self.file.write("Sample: \"prompty\" \n")
    self.file.write("  FillColor: 5 \n")
    self.file.write("  HistoFiles:  {0}/histograms.Wty,{0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999"))
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Title: \"Other #gamma\" \n")
    self.file.write("  Type: BACKGROUND \n")
    self.file.write("  UseMCstat: TRUE \n")
    self.file.write(" \n")
