from getInfoFromRootFile import *
from np_title_dict import *
from np_dict_tty_ttZ_v1 import *

debug = False 

def get_title_for_simplied_btag(syst_name): # get title for lepton simplified model and btag ones
  splitted_string = syst_name.split("_")
  index_ = splitted_string[len(splitted_string) - 1]
  temp_string = "" 
  for i in range(1,(len(splitted_string)-1)):
    temp_string += splitted_string[i] + "_"
  return (temp_string+index_)

  

def get_title(np_name):
  np_title= ""
  counter = 0
  is_found = False
  for syst in np_dict:
    if np_name in syst:
      np_title= np_dict[syst]
      counter = counter +1
      is_found = True
      if(debug): print("(1) syst name: {} \t NP title : {}".format(np_name, np_title))
    else: continue
  #print("(2) syst name: {} \t np_title: {}, \t counter : {} \n".format(np_name, np_title, counter)) 
  # its fine to have 1/2/4, because up and down are repeated, pseudo-data makes it 4
  if((not is_found)):  print("(2) syst name: {} \t NP title : {}, \t counter : {} \n".format(np_name, np_title, counter)) 
  #counter = 5# for debug
  if(not(counter == 1 or counter == 2 or counter ==4 )):  print("(2) syst name: {} \t NP title : {}, \t counter : {} \n".format(np_name, np_title, counter)) 
  return np_title 


dict_sub_category= {}
dict_sub_category["MUON"] = "Leptons"
dict_sub_category["photonSF"] = "Photon"
dict_sub_category["leptonSF"] = "Leptons"
dict_sub_category["MET"] = "MET"
dict_sub_category["JET"] = "Jets"
dict_sub_category["EG_RESOLUTION"] = "Egamma"
dict_sub_category["EG_SCALE"] = "Egamma"
dict_sub_category["pileup"] = "Pileup"
dict_sub_category["weight_jvt"] = "Jets"

def find_syst_category(syst_name):
  sub_category = ""
  counter = 0
  is_found = False
  for syst in dict_sub_category:
    if syst in syst_name:
      sub_category = dict_sub_category[syst]
      counter = counter +1
      is_found = True
      if(debug): print("(1) syst name: {} \t sub_category : {}".format(syst_name, sub_category))
    else: continue
  #print("(2) syst name: {} \t sub_category : {}, \t counter : {} \n".format(syst_name, sub_category, counter)) 
  if((not is_found) or (counter > 1)):  print("(2) syst name: {} \t sub_category : {}, \t counter : {} \n".format(syst_name, sub_category, counter)) 
  return sub_category


class WriteOtherSyst:
  def write_addition_met_syst(self):
    self.file.write(" \n")
    syst_name = "MET_SoftTrk_ResoPara"
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixFileUp: {}/{} \n".format(syst_name+"_9999",self.response_matrix_file_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFileUp:  {0}/histograms.ttgamma_dec  \n".format(syst_name+"_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}/histograms.Wty, {0}/histograms.Wt_inclusive, {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format(syst_name+"_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    syst_name = "MET_SoftTrk_ResoPerp"

    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixFileUp: {}/{} \n".format(syst_name+"_9999",self.response_matrix_file_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFileUp:  {0}/histograms.ttgamma_dec  \n".format(syst_name+"_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}/histograms.Wty ,{0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format(syst_name+"_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: ONESIDED\n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

  ## ********************************************************************************************************************
  # write luminosity uncertainty in the config
  def write_luminosity(self):
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: lumi \n")
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["lumi"]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict["lumi"]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict["lumi"]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict["lumi"]["Title"]))
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: +0.0083 \n")
    self.file.write("  OverallDown: -0.0083 \n")
    self.file.write("  Regions: all \n")
    self.file.write("  Samples: tty_prod\n")
    self.file.write(" \n")

    self.file.write("Systematic: lumi \n")
    self.file.write("  NuisanceParameter: {} \n".format(np_dict["lumi"]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict["lumi"]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict["lumi"]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict["lumi"]["Title"]))
    self.file.write("  Type: OVERALL \n")
    self.file.write("  OverallUp: +0.0083 \n")
    self.file.write("  OverallDown: -0.0083 \n")
    self.file.write("  Regions: all \n")
    self.file.write("  Samples: all \n")
    self.file.write(" \n")

    sub_category = "Jets"
    syst_name =  "JET_PunchThrough_AFII__1"
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}{1}/histograms.Wty , nominal_9999/histograms.Wt_inclusive,nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesDown:  {0}{1}/histograms.Wty, nominal_9999/histograms.Wt_inclusive , nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    syst_name =  "JET_JER_DataVsMC_AFII__1"
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}{1}/histograms.Wty , nominal_9999/histograms.Wt_inclusive,nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesUpSubtractSample:  {0}{1}_{2}/histograms.Wty, nominal_9999/histograms.Wt_inclusive , nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up", "PseudoData_9999"))
    self.file.write("  HistoFilesDown:  {0}{1}/histograms.Wty, nominal_9999/histograms.Wt_inclusive , nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesDownSubtractSample:  {0}{1}_{2}/histograms.Wty,nominal_9999/histograms.Wt_inclusive , nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "down", "PseudoData_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    syst_name =  "JET_RelativeNonClosure_AFII__1"
    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}{1}/histograms.Wty , nominal_9999/histograms.Wt_inclusive,nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesDown:  {0}{1}/histograms.Wty, nominal_9999/histograms.Wt_inclusive , nominal_9999/histograms.wjets , nominal_9999/histograms.wgamma , nominal_9999/histograms.zjets , nominal_9999/histograms.zgamma , nominal_9999/histograms.ttbar , nominal_9999/histograms.diboson , nominal_9999/histograms.ttv , nominal_9999/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")


    # write other normalization uncertainty
    uncalib_jet_name_list = ["uncalib_ljet","uncalib_cjet","uncalib_bjet"]
    for uncalib_jet_name in uncalib_jet_name_list:
      self.file.write("UnfoldingSystematic: {} \n".format(uncalib_jet_name))
      self.file.write("  NuisanceParameter: {} \n ".format(np_dict[uncalib_jet_name]["ttZ_np_name"]))
      self.file.write("  Category: {} \n ".format(np_dict[uncalib_jet_name]["Category"]))
      self.file.write("  SubCategory: {} \n ".format(np_dict[uncalib_jet_name]["SubCategory"]))
      self.file.write("  Title: {} \n ".format(np_dict[uncalib_jet_name]["Title"]))
      self.file.write("  ResponseMatrixFilesUp: nominal_9999/histograms.ttgamma_prod, nominal_9999/histograms.ttgamma_prod_{}_x0.5\n ".format(uncalib_jet_name))
      self.file.write("  Symmetrisation: ONESIDED\n ")
      self.file.write("  Samples: tty_prod\n ")
      self.file.write("  Regions: all  \n ")
      self.file.write(" \n")

      self.file.write("Systematic: {} \n".format(uncalib_jet_name))
      self.file.write("  NuisanceParameter: {} \n ".format(np_dict[uncalib_jet_name]["ttZ_np_name"]))
      self.file.write("  Category: {} \n ".format(np_dict[uncalib_jet_name]["Category"]))
      self.file.write("  SubCategory: {} \n ".format(np_dict[uncalib_jet_name]["SubCategory"]))
      self.file.write("  Title: {} \n ".format(np_dict[uncalib_jet_name]["Title"]))
      self.file.write("  HistoFilesUp: nominal_9999/histograms.ttgamma_dec, nominal_9999/histograms.ttgamma_dec_{}_x0.5\n ".format(uncalib_jet_name))
      self.file.write("  Symmetrisation: ONESIDED\n ")
      self.file.write("  Samples: tty_dec\n ")
      self.file.write("  Regions: all  \n ")
      self.file.write(" \n")
 
      self.file.write("Systematic: {} \n".format(uncalib_jet_name))
      self.file.write("  NuisanceParameter: {} \n ".format(np_dict[uncalib_jet_name]["ttZ_np_name"]))
      self.file.write("  Category: {} \n ".format(np_dict[uncalib_jet_name]["Category"]))
      self.file.write("  SubCategory: {} \n ".format(np_dict[uncalib_jet_name]["SubCategory"]))
      self.file.write("  Title: {} \n ".format(np_dict[uncalib_jet_name]["Title"]))
      self.file.write("  HistoFilesUp: {0}/histograms.Wty,{0}/histograms.Wt_inclusive, {0}/histograms.wjets, {0}/histograms.wgamma, {0}/histograms.zjets, {0}/histograms.zgamma, {0}/histograms.ttbar, {0}/histograms.diboson, {0}/histograms.ttv, {0}/histograms.singletop, {0}/histograms.Wty_{1}_x0.5,{0}/histograms.Wt_inclusive_{1}_x0.5, {0}/histograms.wjets_{1}_x0.5, {0}/histograms.wgamma_{1}_x0.5, {0}/histograms.zjets_{1}_x0.5, {0}/histograms.zgamma_{1}_x0.5, {0}/histograms.ttbar_{1}_x0.5, {0}/histograms.diboson_{1}_x0.5, {0}/histograms.ttv_{1}_x0.5, {0}/histograms.singletop_{1}_x0.5  \n".format("nominal_9999",uncalib_jet_name))
      self.file.write("  Symmetrisation: ONESIDED\n ")
      self.file.write("  Samples: prompty\n ")
      self.file.write("  Regions: all  \n ")
      self.file.write(" \n")
 
  ## ********************************************************************************************************************
  #def write_config_datadriven_syst(self, syst_name):
  def write_config_datadriven_syst(self):
    self.file.write(" \n")
    syst_name="efakeSF_"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  HistoFileUp: {}Up_9999/{} \n".format(syst_name, "histograms.efake"))
    self.file.write("  HistoFileDown: {}Down_9999/{} \n".format(syst_name, "histograms.efake"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Samples: tty_dec, efake, hfake,prompty,Wty\n")#ToDo it should not matter if I include the other samples
    self.file.write("  Samples: efake \n")#ToDo it should not matter if I include the other samples
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")

    self.file.write(" \n")
    syst_name="hfakeSF_"
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  HistoFileUp: {}Up_9999/{} \n".format(syst_name, "histograms.hfake"))
    self.file.write("  HistoFileDown: {}Down_9999/{} \n".format(syst_name, "histograms.hfake"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    #self.file.write("  Samples: tty_dec, efake, hfake,prompty,Wty\n")#ToDo it should not matter if I include the other samples
    self.file.write("  Samples: hfake \n")#ToDo it should not matter if I include the other samples
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")

  ## ********************************************************************************************************************
  ## ******** systematics of objects; these are stored as weights in nominal tree *********
  def write_config_obj_syst(self, syst_name, up_name, down_name):
    sub_category = find_syst_category(syst_name)
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  ResponseMatrixFileUp: {}{}/{} \n".format(syst_name, up_name+"_9999",self.response_matrix_file_name))
    self.file.write("  ResponseMatrixFileDown: {}{}/{} \n".format(syst_name, down_name+"_9999", self.response_matrix_file_name))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFileUp: {}{}/{} \n".format(syst_name,up_name+"_9999", "histograms.ttgamma_dec"))
    self.file.write("  HistoFileDown: {}{}/{} \n".format(syst_name,down_name+"_9999", "histograms.ttgamma_dec"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}{1}/histograms.Wty, {0}{1}/histograms.Wt_inclusive , {0}{1}/histograms.wjets , {0}{1}/histograms.wgamma , {0}{1}/histograms.zjets , {0}{1}/histograms.zgamma , {0}{1}/histograms.ttbar , {0}{1}/histograms.diboson , {0}{1}/histograms.ttv , {0}{1}/histograms.singletop  \n".format(syst_name, up_name+"_9999"))
    self.file.write("  HistoFilesDown:  {0}{1}/histograms.Wty, {0}{1}/histograms.Wt_inclusive , {0}{1}/histograms.wjets , {0}{1}/histograms.wgamma , {0}{1}/histograms.zjets , {0}{1}/histograms.zgamma , {0}{1}/histograms.ttbar , {0}{1}/histograms.diboson , {0}{1}/histograms.ttv , {0}{1}/histograms.singletop  \n".format(syst_name, down_name+"_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write(" \n")

  ## ********************************************************************************************************************
  ## btag
  def write_btag_and_lepton_syst(self):
    ### weight syst with vectors
    syst_weight_vecs={}
    
    #syst_weight_vecs["weight_bTagSF_DL1r_85_eigenvars_B"] = 9
    #syst_weight_vecs["weight_bTagSF_DL1r_85_eigenvars_C"] = 4
    #syst_weight_vecs["weight_bTagSF_DL1r_85_eigenvars_Light"] = 4
    #syst_weight_vecs["weight_bTagSF_DL1r_85_eigenvars_B_down"] = 9
    #syst_weight_vecs["weight_bTagSF_DL1r_85_eigenvars_C_down"] = 4
    #syst_weight_vecs["weight_bTagSF_DL1r_85_eigenvars_Light_down"] = 4
    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_B"] = 45
    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_C"] = 20
    syst_weight_vecs["weight_bTagSF_DL1r_Continuous_eigenvars_Light"] = 20


    up_name = "up"
    down_name = "down"

    self.file.write(" \n")
    for syst in syst_weight_vecs:
      for index_ in range(0,syst_weight_vecs[syst]):
        syst_name = syst + "_{}".format(index_)
        syst_name_up = syst + "_up_{}".format(index_)
        syst_name_down = syst + "_down_{}".format(index_)
        self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
        self.file.write("  Samples: tty_prod \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  ResponseMatrixFileUp: {}/{} \n".format(syst_name_up, self.response_matrix_file_name)) # no 9999 extension; because the whole 9999 index bussiness came to index this. the index for that NP (vector from weight branch) will be used
        self.file.write("  ResponseMatrixFileDown: {}/{} \n".format(syst_name_down,  self.response_matrix_file_name))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
        self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
        self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write(" \n")
        self.file.write("Systematic: {} \n".format(syst_name))
        self.file.write("  Samples: tty_dec \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  HistoFileUp: {}/{} \n".format(syst_name_up, "histograms.ttgamma_dec"))
        self.file.write("  HistoFileDown: {}/{} \n".format(syst_name_down, "histograms.ttgamma_dec"))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
        self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
        self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write("  Type: HISTO \n")
        self.file.write(" \n")

        self.file.write("Systematic: {} \n".format(syst_name))
        self.file.write("  Samples: prompty\n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  HistoFilesUp:  {0}/histograms.Wty, {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format(syst_name_up))
        self.file.write("  HistoFilesDown:  {0}/histograms.Wty , {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format(syst_name_down))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
        self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
        self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write("  Type: HISTO \n")
        self.file.write(" \n")

    syst_weight_vecs_leps={}
    syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Reco"] = 24
    #syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Reco_DOWN"] = 24
    syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_ID"] = 32
    #syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_ID_DOWN"] = 32 
    syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Iso"] = 17
    #syst_weight_vecs_leps["weight_leptonSF_EL_SF_SIMPLIFIED_Iso_DOWN"] = 17

    up_name = "UP"
    down_name = "DOWN"

    for syst in syst_weight_vecs_leps:
      for index_ in range(0,syst_weight_vecs_leps[syst]):
        syst_name = syst + "_{}".format(index_)
        syst_name_up = syst + "_UP_{}".format(index_)
        syst_name_down = syst + "_DOWN_{}".format(index_)
        self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
        self.file.write("  Samples: tty_prod \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  ResponseMatrixFileUp: {}/{} \n".format(syst_name_up, self.response_matrix_file_name)) # no 9999 extension; because the whole 9999 index bussiness came to index this. the index for that NP (vector from weight branch) will be used
        self.file.write("  ResponseMatrixFileDown: {}/{} \n".format(syst_name_down,  self.response_matrix_file_name))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
        self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
        self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write(" \n")

        self.file.write(" \n")
        self.file.write("Systematic: {} \n".format(syst_name))
        self.file.write("  Samples: tty_dec \n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  HistoFileUp: {}/{} \n".format(syst_name_up, "histograms.ttgamma_dec"))
        self.file.write("  HistoFileDown: {}/{} \n".format(syst_name_down, "histograms.ttgamma_dec"))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
        self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
        self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write("  Type: HISTO \n")
        self.file.write(" \n")

        self.file.write(" \n")
        self.file.write("Systematic: {} \n".format(syst_name))
        self.file.write("  Samples: prompty\n")
        self.file.write("  Regions: {} \n".format(self.regions))
        self.file.write("  HistoFilesUp:  {0}/histograms.Wty, {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format(syst_name_up))
        self.file.write("  HistoFilesDown:  {0}/histograms.Wty , {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format(syst_name_down))
        self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
        self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
        self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
        self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
        self.file.write("  Symmetrisation: TWOSIDED \n")
        self.file.write("  Type: HISTO \n")
        self.file.write(" \n")


  ## ********************************************************************************************************************
  ## **** Tree systematics ************
  def write_config_syst(self, syst_name):
    ##*** The reason there is no HistoPathUp/Down in the Unfolding systematics is that
    ##    The particle level will be folded with the response matrix to give the reco spectra?
    ##***
    sub_category=find_syst_category(syst_name)
    
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixFileUp: {}up_9999/{} \n".format(syst_name,self.response_matrix_file_name))
    self.file.write("  ResponseMatrixFileDown: {}down_9999/{} \n".format(syst_name, self.response_matrix_file_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFileUp: {}up_9999/{} \n".format(syst_name, "histograms.ttgamma_dec"))
    self.file.write("  HistoFileDown: {}down_9999/{} \n".format(syst_name, "histograms.ttgamma_dec"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}{1}/histograms.Wty, {0}{1}/histograms.Wt_inclusive, {0}{1}/histograms.wjets , {0}{1}/histograms.wgamma , {0}{1}/histograms.zjets , {0}{1}/histograms.zgamma , {0}{1}/histograms.ttbar , {0}{1}/histograms.diboson , {0}{1}/histograms.ttv , {0}{1}/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesDown:  {0}{1}/histograms.Wty, {0}{1}/histograms.Wt_inclusive, {0}{1}/histograms.wjets , {0}{1}/histograms.wgamma , {0}{1}/histograms.zjets , {0}{1}/histograms.zgamma , {0}{1}/histograms.ttbar , {0}{1}/histograms.diboson , {0}{1}/histograms.ttv , {0}{1}/histograms.singletop  \n".format(syst_name, "down_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")



  ## ********************************************************************************************************************
  def write_config_syst_JET_JER(self, syst_name):
    ##*** The reason there is no HistoPathUp/Down in the Unfolding systematics is that
    ##    The particle level will be folded with the response matrix to give the reco spectra?
    ##***
    sub_category = "Jets"
    self.file.write(" \n")
    self.file.write("UnfoldingSystematic: \"{}\" \n".format(syst_name))
    self.file.write("  ResponseMatrixFileUp: {}up_9999/{} \n".format(syst_name, self.response_matrix_file_name))
    self.file.write("  ResponseMatrixFileUpSubtractSample: {}up_{}/{} \n".format(syst_name,"PseudoData"+"_9999", self.response_matrix_file_name))
    self.file.write("  ResponseMatrixFileDown: {}down_9999/{} \n".format(syst_name, self.response_matrix_file_name))
    self.file.write("  ResponseMatrixFileDownSubtractSample: {}down_{}/{} \n".format(syst_name,"PseudoData"+"_9999", self.response_matrix_file_name))
    self.file.write("  Samples: tty_prod \n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: {} \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: tty_dec\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFileUp: {}up_9999/{} \n".format(syst_name, self.histo_file_name))
    self.file.write("  HistoFileUpSubtractSample: {}up_{}_9999/{} \n".format(syst_name,"PseudoData", "histograms.ttgamma_dec"))
    self.file.write("  HistoFileDown: {}down_9999/{} \n".format(syst_name, self.histo_file_name))
    self.file.write("  HistoFileDownSubtractSample: {}down_{}_9999/{} \n".format(syst_name,"PseudoData", "histograms.ttgamma_dec"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")

    self.file.write(" \n")
    self.file.write("Systematic: {} \n".format(syst_name))
    self.file.write("  Samples: prompty\n")
    self.file.write("  Regions: {} \n".format(self.regions))
    self.file.write("  HistoFilesUp:  {0}{1}/histograms.Wty , {0}{1}/histograms.Wt_inclusive,{0}{1}/histograms.wjets , {0}{1}/histograms.wgamma , {0}{1}/histograms.zjets , {0}{1}/histograms.zgamma , {0}{1}/histograms.ttbar , {0}{1}/histograms.diboson , {0}{1}/histograms.ttv , {0}{1}/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesUpSubtractSample:  {0}{1}_{2}/histograms.Wty, {0}{1}_{2}/histograms.Wt_inclusive , {0}{1}_{2}/histograms.wjets , {0}{1}_{2}/histograms.wgamma , {0}{1}_{2}/histograms.zjets , {0}{1}_{2}/histograms.zgamma , {0}{1}_{2}/histograms.ttbar , {0}{1}_{2}/histograms.diboson , {0}{1}_{2}/histograms.ttv , {0}{1}_{2}/histograms.singletop  \n".format(syst_name, "up", "PseudoData_9999"))
    self.file.write("  HistoFilesDown:  {0}{1}/histograms.Wty, {0}{1}/histograms.Wt_inclusive , {0}{1}/histograms.wjets , {0}{1}/histograms.wgamma , {0}{1}/histograms.zjets , {0}{1}/histograms.zgamma , {0}{1}/histograms.ttbar , {0}{1}/histograms.diboson , {0}{1}/histograms.ttv , {0}{1}/histograms.singletop  \n".format(syst_name, "up_9999"))
    self.file.write("  HistoFilesDownSubtractSample:  {0}{1}_{2}/histograms.Wty,{0}{1}_{2}/histograms.Wt_inclusive , {0}{1}_{2}/histograms.wjets , {0}{1}_{2}/histograms.wgamma , {0}{1}_{2}/histograms.zjets , {0}{1}_{2}/histograms.zgamma , {0}{1}_{2}/histograms.ttbar , {0}{1}_{2}/histograms.diboson , {0}{1}_{2}/histograms.ttv , {0}{1}_{2}/histograms.singletop  \n".format(syst_name, "down", "PseudoData_9999"))
    self.file.write("  NuisanceParameter: {} \n".format(np_dict[syst_name]["ttZ_np_name"]))
    self.file.write("  Category: {} \n".format(np_dict[syst_name]["Category"]))
    self.file.write("  SubCategory: {} \n".format(np_dict[syst_name]["SubCategory"]))
    self.file.write("  Title: \"{}\" \n".format(np_dict[syst_name]["Title"]))
    self.file.write("  Symmetrisation: TWOSIDED \n")
    self.file.write("  Type: HISTO \n")
    self.file.write("  Smoothing: 40 \n")
    self.file.write(" \n")



  ## ********************************************************************************************************************
  def write_whole_config(self):
    self.write_config()
    obj = ReadFromRootFile(self.one_ntuple_file_for_reading_trees)
    tree_list = obj.get_list_of_trees() # list containing tree names
    list_of_obj_syst = obj.get_list_of_uncer_from_nominal()
    #list_of_obj_syst = ["weight_pileup_UP","weight_pileup_DOWN","weight_leptonSF_EL_SF_Trigger_UP","weight_leptonSF_EL_SF_Trigger_DOWN","weight_leptonSF_EL_SF_Reco_UP","weight_leptonSF_EL_SF_Reco_DOWN","weight_leptonSF_EL_SF_ID_UP","weight_leptonSF_EL_SF_ID_DOWN","weight_leptonSF_EL_SF_Isol_UP","weight_leptonSF_EL_SF_Isol_DOWN","weight_leptonSF_MU_SF_Trigger_STAT_UP","weight_leptonSF_MU_SF_Trigger_STAT_DOWN","weight_leptonSF_MU_SF_Trigger_SYST_UP","weight_leptonSF_MU_SF_Trigger_SYST_DOWN","weight_leptonSF_MU_SF_ID_STAT_UP","weight_leptonSF_MU_SF_ID_STAT_DOWN","weight_leptonSF_MU_SF_ID_SYST_UP","weight_leptonSF_MU_SF_ID_SYST_DOWN","weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP","weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN","weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP","weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN","weight_leptonSF_MU_SF_Isol_STAT_UP","weight_leptonSF_MU_SF_Isol_STAT_DOWN","weight_leptonSF_MU_SF_Isol_SYST_UP","weight_leptonSF_MU_SF_Isol_SYST_DOWN","weight_leptonSF_MU_SF_TTVA_STAT_UP","weight_leptonSF_MU_SF_TTVA_STAT_DOWN","weight_leptonSF_MU_SF_TTVA_SYST_UP","weight_leptonSF_MU_SF_TTVA_SYST_DOWN","weight_photonSF_ID_UP","weight_photonSF_ID_DOWN","weight_photonSF_effIso_UP","weight_photonSF_effIso_DOWN","weight_jvt_UP","weight_jvt_DOWN","weight_bTagSF_DL1r_85_eigenvars_B_up","weight_bTagSF_DL1r_85_eigenvars_C_up","weight_bTagSF_DL1r_85_eigenvars_Light_up","weight_bTagSF_DL1r_85_eigenvars_B_down","weight_bTagSF_DL1r_85_eigenvars_C_down","weight_bTagSF_DL1r_85_eigenvars_Light_down","weight_bTagSF_DL1r_85_extrapolation_up","weight_bTagSF_DL1r_85_extrapolation_down","weight_bTagSF_DL1r_85_extrapolation_from_charm_up","weight_bTagSF_DL1r_85_extrapolation_from_charm_down"]

    match_list=['down', 'DOWN', 'Down']
    Dict = {"UP":"DOWN", "Up":"Down", "up":"down"}
    ignore_list_btag = ["DL1r_70","DL1r_77","DL1r_85","DL1r_Continuous", "SIMPLIFIED", "weight_leptonSF_EL_SF_Reco_","weight_leptonSF_EL_SF_ID_","weight_leptonSF_EL_SF_Isol_" ] #ignoring DL1r and SIMPLIFIED systematics; we include it later, these syst contains many NPs with vector indexed
    ignore_list_fakes = ["leptonFake", "efake", "hfake"]
  
    list_of_datadriven_syst = ["efakeSF_Up","efakeSF_Down","hfakeSF_Up","hfakeSF_Down"]
    if(self.all_syst):
      previous_syst = ""
      for tree in tree_list:
        # only use up and generate down with that tree name
        if(tree == 'nominal'): continue
        if(tree == 'truth'): continue
        if 'MET_SoftTrk_ResoP' in tree: continue # added by write_additonal_met_syst()
        if not any(x in tree for x in match_list):
          if "PseudoData" in tree: continue
          if "JET_JER" in tree: # use PseudoData samples for JET_JER
            current_syst = tree
            if(previous_syst == current_syst): continue
            self.write_config_syst_JET_JER(tree[:-2])
            previous_syst = current_syst
          else: # other than JET_JER syst
            current_syst = tree
            if(previous_syst == current_syst): continue
            self.write_config_syst(tree[:-2])
            previous_syst = current_syst

      for branch in list_of_obj_syst: # generate object systematics
        if "weight_photonSF_Trigger_UNCERT_" in branch: continue
        if any(x in branch for x in ignore_list_btag): continue
        if any(x in branch for x in ignore_list_fakes): continue
        if not any(x in branch for x in match_list):
          split_branch_str = branch.split("_") # split the string branch and get last two letter, it should be "UP/Up/up"
  
          str_containting_up = split_branch_str[len(split_branch_str)-1]
          if(self.debug): print("{}: {}".format(str_containting_up, Dict[str_containting_up]) )
          self.write_config_obj_syst(branch[:-2], str_containting_up, Dict[str_containting_up])
  
      # Fakes (data-driven)
      self.write_config_datadriven_syst()
      # write config for sample modelling
      self.write_config_modelling()
      # adding normalization uncertainty
      self.write_bkg_norm_uncertainty()
      ## write lumi
      self.write_luminosity()
      ## write muR_muF
      self.write_muR_muF()
      ## write additonal met syst
      self.write_addition_met_syst()
      ## write btag syst
      self.write_btag_and_lepton_syst()
