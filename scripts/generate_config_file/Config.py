#!/usr/bin/env python
"""configuration file
"""

""" Dictionary with file names
"""
histogram_dict = {
    "histograms_data": "histograms.data"
    , "histograms_ttgamma_prod": "histograms.ttgamma_prod"
    , "histograms_ttgamma_dec": "histograms.ttgamma_dec"
    , "histograms_efake": "histograms.efake"
    , "histograms_efake_tty_prod": "histograms.efake_tty_NLO_prod"
    , "histograms_efake_ttgamma_dec": "histograms.efake_tty_LO_dec"
    , "histograms_hfake": "histograms.hfake"
    , "histograms_hfake_ttgamma_prod": "histograms.hfake_tty_NLO_prod"
    , "histograms_hfake_ttgamma_dec": "histograms.hfake_tty_LO_dec"
    , "histograms_Wty": "histograms.Wty"
    , "histograms_Wt_inclusive": "histograms.Wt_inclusive"
    , "histograms_wjets": "histograms.wjets"
    , "histograms_wgamma": "histograms.wgamma"
    , "histograms_zjets": "histograms.zjets"
    , "histograms_zgamma": "histograms.zgamma"
    , "histograms_ttbar": "histograms.ttbar"
    , "histograms_diboson": "histograms.diboson"
    , "histograms_ttv": "histograms.ttv"
    , "histograms_singletop": "histograms.singletop"
}

normalized_xsec_unfolding= False 
normalized_bin_n_minus_1 = False 
absolute_xsec_unfolding = True 
input_histograms = "/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/"
unfolding_name = False
