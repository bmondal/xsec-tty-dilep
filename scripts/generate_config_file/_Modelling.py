from np_dict_tty_ttZ_v1 import *
class Modelling:
  def write_config_modelling(self):
    # ttgamma_prod var3c
    if(self.ttgamma_prod_var3c):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_var3c"))
      self.file.write("  Samples: tty_prod \n")
      self.file.write("  ResponseMatrixFileUp: {}/{} \n".format("nominal_9999","histograms.ttgamma_prod_var3cUp"))
      self.file.write("  ResponseMatrixFileDown: {}/{} \n".format("nominal_9999","histograms.ttgamma_prod_var3cDown"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_var3c"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_var3c"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_var3c"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_var3c"]["Title"]))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")

    #tty_dec var3c
    if(self.ttgamma_dec_var3c):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("tty_dec_var3c"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_var3c"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_var3c"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_var3c"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_var3c"]["Title"]))
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.tty_dec_var3c_up"))
      self.file.write("  HistoFileDown: {}/{} \n".format("nominal_9999","histograms.tty_dec_var3c_down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: tty_dec\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("\n")

    # tty_dec FSR
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("tty_dec_fsr"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_fsr"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_fsr"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_fsr"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_fsr"]["Title"]))
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.tty_dec_fsr_up"))
      self.file.write("  HistoFileDown: {}/{} \n".format("nominal_9999","histograms.tty_dec_fsr_down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: tty_dec\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("\n")


    #prompty ttbar var3c
    if(self.prompty_ttbar_var3c):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("ttbar_var3c"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_var3c"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["ttbar_var3c"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_var3c"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_var3c"]["Title"]))
      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.Wty, {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.tt_var3c_up"))
      self.file.write("  HistoFilesDown:  {0}/{1} , {0}/histograms.Wty,{0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.tt_var3c_down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))

    # ttgamma_prod Herwig7
    if(self.ttgamma_prod_herwig7):
      self.file.write(" \n")
      self.file.write("UnfoldingSystematic: {} \n".format("tty_prod_Herwig7"))
      self.file.write("  Samples: tty_prod\n")
      self.file.write("  ResponseMatrixFileUp: {}/{} \n".format("nominal_9999","histograms.ttgamma_prod_H7"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_Herwig7"]["Title"]))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # tty_dec Herwig7
    if(self.ttgamma_dec_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("tty_dec_Herwig7"))
      self.file.write("  Samples: tty_dec\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_Herwig7"]["Title"]))
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.ttgamma_dec_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # prompty ttbar Herwig7
    if(self.prompty_ttbar_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_ttbar_Herwig7"))
      self.file.write("  Samples: prompty\n")
      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.Wty, {0}/histograms.Wt_inclusive  , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.promptY_ttbar_H7"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_ttbar_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["prompty_ttbar_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_ttbar_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_ttbar_Herwig7"]["Title"]))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


    # prompty Wty Herwig7
    if(self.prompty_wty_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("Wty_Herwig7"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_Wty_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["prompty_Wty_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_Wty_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_Wty_Herwig7"]["Title"]))
      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.wjets, {0}/histograms.Wt_inclusive  , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.Wty_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples:  prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")

    # prompty Wt Herwig7
    if(self.prompty_wty_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("Wt_Herwig7"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_Wt_Herwig7"]["ttZ_np_name"]))
      self.file.write("  Category: {} \n".format(np_dict["prompty_Wt_Herwig7"]["Category"]))
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_Wt_Herwig7"]["SubCategory"]))
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_Wt_Herwig7"]["Title"]))
      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.wjets, {0}/histograms.Wty, {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.ttbar , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.Wt_inclusive_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples:  prompty\n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")

    # efake tty_prod Herwig7
    if(self.efake_ttgamma_prod_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_tty_prod_Herwig7"))
      self.file.write("  Samples:  efake\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_Herwig7"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_Herwig7"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_Herwig7"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_Herwig7"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_tty_prod_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # efake tty_dec Herwig7
    if(self.efake_ttgamma_dec_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_tty_dec_Herwig7"))
      self.file.write("  Samples:  efake\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_dec_Herwig7"]["ttZ_np_name"])) # naming used to correlate the systematics
      self.file.write("  Category: {} \n".format(np_dict["tty_dec_Herwig7"]["Category"])) # naming used to correlate the systematics
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_dec_Herwig7"]["SubCategory"])) # naming used to correlate the systematics
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_dec_Herwig7"]["Title"])) # naming used to correlate the systematics
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_tty_dec_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # efake ttbar Herwig7
    if(self.efake_ttbar_herwig7):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_ttbar_Herwig7"))
      self.file.write("  Samples:  efake\n")
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_Herwig7"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["ttbar_Herwig7"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_Herwig7"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_Herwig7"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_ttbar_H7"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # efake ttbar hdamp_var 
    if(self.efake_ttbar_hdamp):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_ttbar_hdamp_var"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_hdamp_var"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["ttbar_hdamp_var"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_hdamp_var"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_hdamp_var"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_ttbar_hdamp_var"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: efake \n")
      self.file.write("  Regions: {} \n".format(self.regions))
    # efake ttbar aMC 
#    if(self.efake_ttbar_aMC):
#      self.file.write(" \n")
#      self.file.write("Systematic: {} \n".format("efake_ttbar_aMC"))
#      self.file.write("  NuisanceParameter: {} \n".format("ttbar_aMC")) # correlated
#      self.file.write("  Category: {} \n".format("ttbar_aMC")) # correlated
#      self.file.write("  SubCategory: {} \n".format("ttbar_aMC")) # correlated
#      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_ttbar_aMC"))
#      self.file.write("  Symmetrisation: ONESIDED \n")
#      self.file.write("  Title: \"{}\" \n".format("ttbar_aMC"))
#      self.file.write("  Type: HISTO \n")
#      self.file.write("  Samples: efake \n")
#      self.file.write("  Regions: {} \n".format(self.regions))

    # efake ttbar pt_hard
    if(self.efake_ttbar_aMC):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_ttbar_pt_hard"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["ttbar_pt_hard"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["ttbar_pt_hard"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["ttbar_pt_hard"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["ttbar_pt_hard"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_ttbar_pt_hard"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: efake \n")
      self.file.write("  Regions: {} \n".format(self.regions))

    # efake tty_prod var3c 
    if(self.efake_ttgamma_prod_var3c):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("efake_tty_prod_var3c"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["tty_prod_var3c"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["tty_prod_var3c"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["tty_prod_var3c"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["tty_prod_var3c"]["Title"])) # correlated
      self.file.write("  HistoFileUp: {}/{} \n".format("nominal_9999","histograms.efake_tty_prod_var3c_Up"))
      self.file.write("  HistoFileDown: {}/{} \n".format("nominal_9999","histograms.efake_tty_prod_var3c_Down"))
      self.file.write("  Symmetrisation: TWOSIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: efake \n")
      self.file.write("  Regions: {} \n".format(self.regions))


    # promptY ttbar hdamp_var 
    if(self.prompty_ttbar_hdamp):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_ttbar_hdamp_var"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_ttbar_hdamp_var"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["prompty_ttbar_hdamp_var"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_ttbar_hdamp_var"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_ttbar_hdamp_var"]["Title"])) # correlated
      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.Wty,{0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.promptY_ttbar_hdamp_var"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  ReferenceSample: ttbar_AFII \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


#    # prompY ttbar aMC # NO need, using p_T hard instead
#    if(self.prompty_ttbar_aMC):
#      self.file.write(" \n")
#      self.file.write("Systematic: {} \n".format("prompty_ttbar_aMC"))
#      self.file.write("  NuisanceParameter: {} \n".format("ttbar_aMC")) # correlated
#      self.file.write("  Category: {} \n".format("ttbar_aMC")) # correlated
#      self.file.write("  SubCategory: {} \n".format("ttbar_aMC")) # correlated
#      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.Wty, {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.promptY_ttbar_aMC"))
#      self.file.write("  Symmetrisation: ONESIDED \n")
#      self.file.write("  Title: \"{}\" \n".format("ttbar_aMC"))
#      self.file.write("  Type: HISTO \n")
#      self.file.write("  Samples: prompty\n")
#      self.file.write("  ReferenceSample: ttbar_AFII \n")
#      self.file.write("  Regions: {} \n".format(self.regions))

    # prompY ttbar pt_hard
    if(self.prompty_ttbar_pt_hard):
      self.file.write(" \n")
      self.file.write("Systematic: {} \n".format("prompty_ttbar_pt_hard"))
      self.file.write("  NuisanceParameter: {} \n".format(np_dict["prompty_ttbar_pt_hard"]["ttZ_np_name"])) # correlated
      self.file.write("  Category: {} \n".format(np_dict["prompty_ttbar_pt_hard"]["Category"])) # correlated
      self.file.write("  SubCategory: {} \n".format(np_dict["prompty_ttbar_pt_hard"]["SubCategory"])) # correlated
      self.file.write("  Title: \"{}\" \n".format(np_dict["prompty_ttbar_pt_hard"]["Title"])) # correlated
      self.file.write("  HistoFilesUp:  {0}/{1} , {0}/histograms.Wty, {0}/histograms.Wt_inclusive , {0}/histograms.wjets , {0}/histograms.wgamma , {0}/histograms.zjets , {0}/histograms.zgamma , {0}/histograms.diboson , {0}/histograms.ttv , {0}/histograms.singletop  \n".format("nominal_9999","histograms.promptY_ttbar_pt_hard"))
      self.file.write("  Symmetrisation: ONESIDED \n")
      self.file.write("  Type: HISTO \n")
      self.file.write("  Samples: prompty\n")
      self.file.write("  ReferenceSample: ttbar_AFII \n")
      self.file.write("  Regions: {} \n".format(self.regions))
      self.file.write("  Smoothing: 40 \n")
      self.file.write("\n")


