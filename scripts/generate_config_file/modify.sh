sed -i 's/NuisanceParameter: \\"{}\\"/NuisanceParameter: {}/g' _BkgNorm.py 
sed -i 's/NuisanceParameter: \\"{}\\"/NuisanceParameter: {}/g' _WriteOtherSyst.py
sed -i 's/NuisanceParameter: \\"{}\\"/NuisanceParameter: {}/g' _Modelling.py
sed -i 's/NuisanceParameter: \\"{}\\"/NuisanceParameter: {}/g' _MuRMuFpdf.py

python  modify_lines_category_subcategory.py _BkgNorm.py 
python  modify_lines_category_subcategory.py _WriteOtherSyst.py
python  modify_lines_category_subcategory.py _Modelling.py
python  modify_lines_category_subcategory.py _MuRMuFpdf.py
