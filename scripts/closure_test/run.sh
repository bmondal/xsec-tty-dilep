path="$HOME/xsec-tty-dilep//abs-xsec-with-trexfiles-local/v12/v13/stat-all//"
path_truth="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/CR_sig/nominal_9999/"
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_pt_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_eta_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_dr_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_dr1_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_dr2_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_dEtall_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_dPhill_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_ptll_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_drphb_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_drlj_all_stat/ --path-truth  $path_truth
python plot_unfolded_truth.py --path-trex-fit $path/tty2l_ptj1_all_stat/ --path-truth  $path_truth
