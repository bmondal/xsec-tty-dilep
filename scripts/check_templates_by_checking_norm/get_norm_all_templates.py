#!/usr/bin/env python3
import argparse
import os
import ROOT

def get_integral(file, histogram):
    hist = file.Get(histogram)
    return hist.Integral()+hist.GetBinContent(hist.GetNbinsX()+1)

def process_file(file):
    integrals = {}
    integrals["Reco/hist_reco_ph_eta_full_weighted"] = get_integral(file, "Reco/hist_reco_ph_eta_full_weighted")
    #integrals["particle/hist_part_pt"] = get_integral(file, "particle/hist_part_pt")
    return integrals

def process_folder(folder_path): # this returns a list of tuples containing (subfolder, file_name, hist_name, integral)
    results = [] # this is a list of tuples (later appened). tuples are immutable so once created can't change its content
    corrupted_files = []
    is_file_corrupted = False
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if file_path.endswith(".root"):
                if not ("ttgamma_prod" in file_path ): continue # DANGER remove this
                try:
                  file = ROOT.TFile.Open(file_path)
                except:
                  is_file_corrupted = True
                  corrupted_files.append(file_path)
                if file and not is_file_corrupted:
                    subfolder = os.path.relpath(root, folder_path)
                    integrals = process_file(file)
                    for hist_name, integral in integrals.items():
                        result = (subfolder, file_name, hist_name, integral)
                        results.append(result)
    print(">>> corrupted files : \n")
    for item in corrupted_files:
      print("{}\n".format(item))
    return results

def process_folder_return_dict(folder_path): # this returns a dict containing key: tuple(sublder,file_name,hist_name) and value: integral
    results = {}
    for root, dirs, files in os.walk(folder_path):
        for file_name in files:
            file_path = os.path.join(root, file_name)
            if file_path.endswith(".root"):
                file = ROOT.TFile.Open(file_path)
                if file:
                    subfolder = os.path.relpath(root, folder_path)
                    integrals = process_file(file)
                    for hist_name, integral in integrals.items():
                        key = (subfolder, file_name, hist_name)
                        results[key] = integral
    return results

def write_results(results, output_file):
    with open(output_file, "w") as f:
        f.write("subfolder,\t file,\t hist,\t integral\n")
        for result in results:
            f.write(",\t ".join(str(x) for x in result) + "\n")


def write_results_zero_integral(results, output_file):
    with open(output_file, "w") as f:
        f.write("subfolder,\t file,\t hist,\t integral\n")
        for result in results:
            #if not "ttgamma_prod" in result[1] or not "ttgamma_dec" in result[1]: continue
            print(result[3])
            if not(result[3] == 0.0): continue
            f.write(",\t ".join(str(x) for x in result) + "\n")


