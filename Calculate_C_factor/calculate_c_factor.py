# this works only for "Reco" subdirectory

import argparse
import ROOT

debug = False

class AddNormalization:
  def __init__(self, rootfilepath, rootfilename):
    self.rootfilename_sr1 = "{}/CR_sig/{}".format(rootfilepath,rootfilename)
    self.rootfilename_sr2 = "{}/CR_bkg/{}".format(rootfilepath,rootfilename)
    self.file_sr1 = ROOT.TFile(self.rootfilename_sr1)
    self.file_sr2 = ROOT.TFile(self.rootfilename_sr2)

  # create new rootfiles and update
  def print_fiducial_info(self):
    # get the directory object
    histo_reco_sr1 = self.file_sr1.Get("Reco/hist_reco_ph_eta_full_weighted")
    histo_reco_sr2 = self.file_sr2.Get("Reco/hist_reco_ph_eta_full_weighted")
    histo_particle = self.file_sr1.Get("particle/hist_part_ph_eta_full_weighted")
    fiducial_reco_sr1 = histo_reco_sr1.Integral()+histo_reco_sr1.GetBinContent(histo_reco_sr1.GetNbinsX()+1)
    fiducial_reco_sr2 = histo_reco_sr2.Integral()+histo_reco_sr2.GetBinContent(histo_reco_sr2.GetNbinsX()+1)
    fiducial_reco = fiducial_reco_sr1 + fiducial_reco_sr2
    #fiducial_reco = fiducial_reco_sr1
    fiducial_particle = histo_particle.Integral()+histo_particle.GetBinContent(histo_particle.GetNbinsX()+1)
    print("{} \t {}\n".format(round(fiducial_particle,1), round(fiducial_reco,1)))


if __name__=='__main__':
  # add normalization to a test.root file and in test_hist histogram and save as test_normalized.root
  parser = argparse.ArgumentParser()
  parser.add_argument("--rootfilepath", type = str, help="name of the rootfile in which histogram contains")
  parser.add_argument("--rootfilename", type = str, help="name of the rootfile in which histogram contains")
  args = parser.parse_args()

#  histogram_list = ["hist_reco_ph_pt_full_weighted", "hist_reco_ph_eta_full_weighted", 
#  "hist_reco_ph_drphl_full_weighted","hist_reco_ph_drphl2_full_weighted", "hist_reco_ph_drphl1_full_weighted",
#  "hist_reco_dPhill_full_weighted", "hist_reco_dEtall_full_weighted", "hist_reco_Ptll_full_weighted","hist_reco_j1_pt_full_weighted"]

  obj = AddNormalization(args.rootfilepath, args.rootfilename)
  obj.print_fiducial_info()
