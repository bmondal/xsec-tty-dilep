### Directory

#### script
contains various scripts
  - generate_config_file: scripts for generating trex-config
```
source setup.sh
./run_generate_stat.sh: for generating stat only configs
./run_generate_syst.sh: for generating syst all configs

```
  - prepare_whole_fit_scripts: this contains fully prepared script for fitting.
    This includes generating the scripts (previous bullet) and putting the script in right folder. After that preparing a shell script for building and running the whole procedure
```
./run.sh # this contains two functions for stat and syst

```





expired:

python -> Contains python scripts for genrating trex-fitter config files
abs-xsec-with-trexfiles -> for calculating absolute differential xsec. All the trex-fitter workspaces are here including config file used


### Steps:
  - make correct binning and copy to new folder so that the binnign information is not there in the filename
  - correct binning for response matrix
  - Generate trex-config
  - run trex-fitter
