In case of tty_prod+tty_dec trex-fit config generation:
  - the modelling variation of tty_prod should only affect tty_prod sample and shouldn't affect tty_dec sample.
  Creating incl (tty_incl_muR/var3c/PDF/..) is not necessary. Only vary the one and keep the other fixed.
  - Replace tty_dec Systematics with the UnfoldingSystematics
