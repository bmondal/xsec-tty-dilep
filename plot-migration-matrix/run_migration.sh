DISPLAY=0
#pt
path="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/"
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_ph_pt_reco_part_full_weighted", "p_{T}(#gamma) [GeV]","SR1" )'
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_ph_pt_reco_part_full_weighted", " p_{T}(#gamma) [GeV]" ,"SR2" )'
# eta
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_ph_eta_reco_part_full_weighted", "|#eta(#gamma)|", "SR1" )'
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_ph_eta_reco_part_full_weighted", "|#eta(#gamma)|", "SR2" )'
# dr(ph.l)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_ph_drphl_reco_part_full_weighted", "min #Delta R(#gamma, l)", "SR1" ) '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_ph_drphl_reco_part_full_weighted", "min #Delta R(#gamma, l)", "SR2" ) '
# dr(ph,l1)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_ph_drphl1_reco_part_full_weighted", " #Delta R(#gamma, l1)", "SR1" ) '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_ph_drphl1_reco_part_full_weighted", " #Delta R(#gamma, l1)", "SR2" ) '
# dr(ph, l2)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_ph_drphl2_reco_part_full_weighted", " #Delta R(#gamma, l2)", "SR1") '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_ph_drphl2_reco_part_full_weighted", " #Delta R(#gamma, l2)", "SR2") '
# dr(ph, b)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_drphb_reco_part_full_weighted", " min #Delta R(#gamma, b)", "SR1") '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_drphb_reco_part_full_weighted", " min #Delta R(#gamma, b)", "SR2") '
# dEta(ll)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_dEtall_reco_part_full_weighted", " | #Delta #eta (ll) |", "SR1") '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_dEtall_reco_part_full_weighted", " | #Delta #eta (ll) |", "SR2") '
# dPhi(ll)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_dPhill_reco_part_full_weighted", " | #Delta #phi (ll) |", "SR1") '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_dPhill_reco_part_full_weighted", " | #Delta #phi (ll) |", "SR2") '
# pt(ll)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_Ptll_reco_part_full_weighted", " | p_{T} (ll) |", "SR1") '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_Ptll_reco_part_full_weighted", " | p_{T} (ll) |", "SR2") '
# dr(l,j)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_drlj_reco_part_full_weighted", " min #Delta R(l,j)", "SR1") '
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_drlj_reco_part_full_weighted", " min #Delta R(l,j)", "SR2") '
# pt(j1)
root -l -q migration.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_j1_pt_reco_part_full_weighted", "p_{T}(j1) [GeV]","SR1" )'
root -l -q migration.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_j1_pt_reco_part_full_weighted", "p_{T}(j1) [GeV]","SR2" )'
