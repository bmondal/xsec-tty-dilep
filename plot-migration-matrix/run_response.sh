DISPLAY=0
path="/home/bm863639/eos/physics_analysis/tty/Dilepton/Unfolding_samples/Unfolding_inputs_v12_v11/NN06_Nominal_from_fine/"
#pt
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", "p_{T}(#gamma) [GeV]","SR1" )'
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_pt", " p_{T}(#gamma) [GeV]" ,"SR2" )'
# eta
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_eta", "|#eta(#gamma)|", "SR1" )'
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_eta", "|#eta(#gamma)|", "SR2" )'
# dr(ph.l)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl", "min #Delta R(#gamma, l)", "SR1" ) '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl", "min #Delta R(#gamma, l)", "SR2" ) '
# dr(ph,l1)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl1", " #Delta R(#gamma, l1)", "SR1" ) '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl1", " #Delta R(#gamma, l1)", "SR2" ) '
# dr(ph, l2)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl2", " #Delta R(#gamma, l2)", "SR1") '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ph_dryl2", " #Delta R(#gamma, l2)", "SR2") '
# dr(ph, b)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drphb", " min #Delta R(#gamma, b)", "SR1") '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drphb", " min #Delta R(#gamma, b)", "SR2") '
# dEta(ll)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_dEtall", " | #Delta #eta (ll) |", "SR1") '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_dEtall", " | #Delta #eta (ll) |", "SR2") '
# dPhi(ll)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_dPhill", " | #Delta #phi (ll) |", "SR1") '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_dPhill", " | #Delta #phi (ll) |", "SR2") '
# pt(ll)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ptll", " | p_{T} (ll) |", "SR1") '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_ptll", " | p_{T} (ll) |", "SR2") '
# dr(l,j)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drlj", " min #Delta R(l,j)", "SR1") '
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_drlj", " min #Delta R(l,j)", "SR2") '
# pt(j1)
root -l -q response.C'("'${path}'/CR_sig/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_j1_pt", "p_{T}(j1) [GeV]","SR1" )'
root -l -q response.C'("'${path}'/CR_bkg/nominal_9999/histograms.ttgamma_prod.root","h2_response_matrix_j1_pt", "p_{T}(j1) [GeV]","SR2" )'
